package com.example.mock2_project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mock2ProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Mock2ProjectApplication.class, args);
    }

}
