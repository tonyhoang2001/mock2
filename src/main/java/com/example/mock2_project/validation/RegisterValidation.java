package com.example.mock2_project.validation;

import com.example.mock2_project.dto.request.RegisterRequest;
import com.example.mock2_project.enums.Role;
import com.example.mock2_project.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDate;

@Component
public class RegisterValidation {

    @Autowired
    AccountRepository accountRepository;

    public void validateAccountRegistration(RegisterRequest registerRequest) throws Exception {
        if (accountRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
            throw new Exception("[datdq14_message] Email '" + registerRequest.getEmail() + "' is already exist, try another!");
        }

        if ((registerRequest.getRole() == Role.USER) || (registerRequest.getRole() == Role.ADMIN)) {
            System.out.println("[datdq14_message] Role '" + registerRequest.getRole() + "': accepted");
        } else {
            throw new Exception("[datdq14_message] Role must be USER/ADMIN");
        }

        //System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaxxxxxxxxxxxxxxxxxxxxxxxxxx");
        String stringDOBValidation = registerRequest.getDob();
        LocalDate localDateDOBValidation = LocalDate.parse(stringDOBValidation);
        if (localDateDOBValidation.isAfter(ChronoLocalDate.from(LocalDateTime.now()))) {
            throw new Exception("[datdq14_message] Future date is not allowed");
        }
        /*if (localDateDOBValidation.isAfter(ChronoLocalDate.from(LocalDateTime.of(2005, 04, 19, 00, 00)))) {
            throw new Exception("[datdq14_message] Account must be 18+");
        }*/
        if (localDateDOBValidation.isAfter(ChronoLocalDate.from(LocalDateTime.now().plusSeconds(-18*365*24*60*60)))) {
            throw new Exception("[datdq14_message] Account must be 18+");
        }

        //System.out.println(LocalDateTime.now().plusSeconds(-80L *365*24*60*60));

        if (localDateDOBValidation.isBefore(ChronoLocalDate.from(LocalDateTime.now().plusSeconds(-80L *365*24*60*60)))) {
            throw new Exception("[datdq14_message] Account > 80 year old is not allowed");
        }
        //System.out.println(localDateDOBValidation);
    }
}
