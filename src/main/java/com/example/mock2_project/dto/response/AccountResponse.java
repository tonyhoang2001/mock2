package com.example.mock2_project.dto.response;

import com.example.mock2_project.entity.AccessToken;
import com.example.mock2_project.entity.AccountInfo;
import com.example.mock2_project.entity.RefreshToken;
import com.example.mock2_project.enums.Role;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class AccountResponse {
    private Long id;
    private String email;
    private String fullName;
    private Role role;
    private ResponseFile file;
    private AccountInfo accountInfo;
    private AccountInfoResponse accountInfoResponse;
    private List<AccessToken> accessToken;
    private RefreshToken refreshToken;
    private RefreshTokenResponse refreshTokenResponse;
    private List<RatingResponse> ratingResponses;
    private List<InvoiceResponse> invoiceResponses;

    public AccountResponse(Long id, String email, Role role, String fullName) {
        this.id = id;
        this.email = email;
        this.role = role;
        this.fullName = fullName;
    }

    public AccountResponse(Long id, String email, String fullName, Role role, ResponseFile responseFile,
                           AccountInfo accountInfo, List<AccessToken> accessToken, RefreshToken refreshToken,
                           List<RatingResponse> ratingResponses, List<InvoiceResponse> invoiceResponses) {
        this.id = id;
        this.email = email;
        this.role = role;
        this.fullName = fullName;
        this.file = responseFile;
        this.accountInfo = accountInfo;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.ratingResponses = ratingResponses;
        this.invoiceResponses = invoiceResponses;
    }

    public AccountResponse(Long id, String email, String fullName, Role role, ResponseFile responseFile, AccountInfoResponse accountInfoResponse, List<AccessToken> accessToken, RefreshTokenResponse refreshTokenResponse, List<RatingResponse> ratingResponses, List<InvoiceResponse> invoiceResponses) {
        this.id = id;
        this.email = email;
        this.role = role;
        this.fullName = fullName;
        this.file = responseFile;
        this.accountInfoResponse = accountInfoResponse;
        this.accessToken = accessToken;
        this.refreshTokenResponse = refreshTokenResponse;
        this.ratingResponses = ratingResponses;
        this.invoiceResponses = invoiceResponses;
    }
}
