package com.example.mock2_project.dto.response;

import com.example.mock2_project.dto.request.InvoiceDetailRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InvoiceResponse {
    private Long id;
    private double total;
    private LocalDate expectedDate;
    private LocalDate shippedDate;
    private List<InvoiceDetailResponse> invoiceDetails;
}
