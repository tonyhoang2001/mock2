package com.example.mock2_project.dto.response;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReLoginResponse {
    private String newAccessToken;
    private String currentRefreshToken;
    private String duration;
    private String remainingTime;
}
