package com.example.mock2_project.dto.response;

import com.example.mock2_project.enums.AccessTokenType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccessTokenResponse {
    private String accessToken;
    private AccessTokenType accessTokenType;
    private boolean isExpired;
    private boolean isRevoked;
}
