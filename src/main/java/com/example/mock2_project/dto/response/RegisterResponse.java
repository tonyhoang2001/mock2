package com.example.mock2_project.dto.response;

import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.entity.RefreshToken;
import com.example.mock2_project.enums.AccessTokenType;
import com.example.mock2_project.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDate;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterResponse {
    private String name;
    private String email;
    private String password;
    private Collection<? extends GrantedAuthority> role;

    //private FileDB fileDB;
    private String phone;
    private String address;
    private LocalDate dob;

    //private String accessToken;
    //private AccessTokenType accessTokenType;
    //private boolean isExpired;
    //private boolean isRevoked;
}
