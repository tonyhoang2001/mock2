package com.example.mock2_project.dto.response;

import com.example.mock2_project.enums.AccessTokenType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class AccountInfoResponse {
    private Long id;
    private String phone;
    private LocalDate dob;
    private String address;

    public AccountInfoResponse(Long id, String phone, LocalDate dob, String address) {
        this.id = id;
        this.phone = phone;
        this.dob = dob;
        this.address = address;
    }
}
