package com.example.mock2_project.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InvoiceDetailResponse {
    private Long id;
    private int quantity;
    private double totalPrice;
    private Long productId;
}
