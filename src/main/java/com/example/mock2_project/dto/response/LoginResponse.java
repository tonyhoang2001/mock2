package com.example.mock2_project.dto.response;

import com.example.mock2_project.entity.RefreshToken;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponse {
    private String newAccessToken;
    private String email;
    private Collection<? extends GrantedAuthority> role;
    private RefreshToken newRefreshToken;
    private String refreshTokenDuration;
    private String refreshTokenRemainingTime;
}
