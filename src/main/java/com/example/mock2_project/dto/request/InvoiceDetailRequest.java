package com.example.mock2_project.dto.request;

import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InvoiceDetailRequest {
    private Long id;
    @Min(value = 0, message = "Invalid quantity")
    private int quantity;
    @DecimalMin(value = "0", message = "Invalid quantity")
    private double totalPrice;
    @Min(value = 1, message = "Invalid product ID")
    private Long productId;
}
