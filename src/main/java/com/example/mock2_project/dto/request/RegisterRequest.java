package com.example.mock2_project.dto.request;

import com.example.mock2_project.entity.AccountInfo;
import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.enums.AccessTokenType;
import com.example.mock2_project.enums.Role;
import com.fasterxml.jackson.annotation.JsonCreator;
import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

//nguon password regex: https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegisterRequest {
    /*account*/
    @Email(message = "[datdq14_message] wrong Email format, need folow format: a@b", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String email;

    /**
     * (?=.*[0-9]) means that the password must contain a single digit from 1 to 9. Thay dau = thanh dau ! neu nhu khong muon include number: (?!.*[0-9])
     * (?=.*\d): Tuong tu [0-9], dai dien cho number
     *
     * (?=.*[a-z]) means that the password must contain one lowercase letter. Thay dau = thanh dau ! neu nhu khong muon include a-z: (?!.*[a-z])
     *
     * (?=.*[A-Z]) means that the password must contain one uppercase letter. Thay dau = thanh dau ! neu nhu khong muon include A-Z: (?!.*[A-Z])
     *
     * (?=.*\W) means that the password must contain one special character. Thay dau = thanh dau ! ⟶ the password must not contain any special characters: (?!.*\W)
     *
     * .{8,16} means that the password must be 8-16 characters long. We must use this at the end of the regex, just before the $ symbol.
     * Remove maximum length restriction: used .{8,} Instead of .{8,16}
     *
     * What are ^ and $: ^ indicates the beginning of the string. $ indicates the end of the string.
     * If we don't use these ^ & $, the regex will not be able to determine the maximum length of the password. In the above example, we have a condition that the password can't be longer than 16 characters, to make that condition work, we have used these ^ & $
     */
    //@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", message = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\\W).{8,16}$", message = "[datdq14_message] Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character")
    private String password;

    @NotBlank(message = "[datdq14_message] Name can not blank")
    private String name;

    //@Pattern(regexp = "ADMIN|USER", message = "Invalid role! Input ADMIN/USER")
    private Role role;

    //private String image;
    //private FileDB avatar;

    /*account infor*/
    //@Min(value = 10, message = "phone number must have fixed 10 numbers")
    //@Max(value = 10, message = "phone number must have fixed 10 numbers")
    @Length(max = 10, min = 10, message = "[datdq14_message] phone number must have fixed 10 numbers")
    @NotBlank(message = "[datdq14_message] Phone number can not blank")
    private String phone;

    @NotBlank(message = "[datdq14_message] Address can not blank")
    private String address;


    //@PastOrPresent(message = "Future date is not allow") //only use for Date/LocalDateTime⁈, cant use for String
    private String dob;

    /*access token*/
    private AccessTokenType accessTokenType;
    private boolean expired;
    private boolean revoked;
}
