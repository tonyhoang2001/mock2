package com.example.mock2_project.dto.request;

import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddingRatingRequest {
    private String comment;
    @Min(value = 1, message = "ID is invalid!")
    private Long productId;
}
