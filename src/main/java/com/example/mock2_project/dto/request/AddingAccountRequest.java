package com.example.mock2_project.dto.request;

import com.example.mock2_project.entity.AccessToken;
import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.entity.RefreshToken;
import com.example.mock2_project.enums.Role;
import jakarta.validation.constraints.*;
import lombok.*;
import org.hibernate.validator.constraints.Length;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AddingAccountRequest {

    @Email(message = "Wrong email syntax", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    private String email;
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", message = "Wrong Password syntax! Password must have at least: 8 characters, include at least 1 CAPITALIZE (A-Z), 1 lowercase (a-z), 1 number (0-9), 1 special symbol ($,&…)")
    private String password;

    @NotBlank(message = "Name can not blank, please fill!")
    private String fullName;

    @Pattern(regexp = "ADMIN|USER", message = "Invalid role! Input ADMIN/USER")
    private Role role;

    private FileDB fileDB;

    @Length(max = 10, min = 10, message = "Wrong number, Phone have only 10 numbers!")
    @Digits(integer = 128, fraction = 10, message = "Wrong number, Please input number!")
    private String phone;
    private String address;

    @NotNull(message = "The date of birth is required")
    @PastOrPresent(message = "Wrong date, Can't input future date")
    private String dob;

    private AccessToken accessToken;
    private RefreshToken refreshToken;
}
