package com.example.mock2_project.dto.request;

import com.example.mock2_project.entity.InvoiceDetail;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.NotFound;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class InvoiceRequest {
    private Long id;
    @DecimalMin(value = "0.0", message = "Invalid total")
    private double total;
    private LocalDate expectedDate;
    private LocalDate shippedDate;
    @NotNull(message = "Must have invoice detail")
    private List<InvoiceDetailRequest> invoiceDetails;
}
