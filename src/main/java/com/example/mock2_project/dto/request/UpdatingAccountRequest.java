package com.example.mock2_project.dto.request;

import com.example.mock2_project.enums.Role;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UpdatingAccountRequest {
    @Min(value = 1, message = "Invalid id!")
    private Long id;
    private String email;
    private String password;
    private String fullName;
    @Pattern(regexp = "ADMIN|USER", message = "Invalid role!")
    private Role role;
}
