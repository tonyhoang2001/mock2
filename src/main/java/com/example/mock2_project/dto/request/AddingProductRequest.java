package com.example.mock2_project.dto.request;

import jakarta.validation.constraints.Min;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AddingProductRequest {
    private String name;
    private String description;
    @Min(value = 0, message = "Invalid price!")
    private double price;
    @Min(value = 0, message = "Invalid quantity!")
    private int quantity;

    @Min(value = 1, message = "Invalid category ID!")
    private Long categoryId;
}
