package com.example.mock2_project.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDate;

@RestControllerAdvice
public class ApiExceptionHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleAllException(Exception ex, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(LocalDate.now(), 500, "Exception: " + ex.getMessage());
        LOGGER.error(errorMessage.toString());
        return errorMessage;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage handleInvalidArguement(MethodArgumentNotValidException ex) {
        ErrorMessage errorMessage = new ErrorMessage(LocalDate.now(), 400, ex.getBindingResult().getFieldErrors().get(0).getDefaultMessage());
        LOGGER.error(errorMessage.toString());
        return errorMessage;
    }

    @ExceptionHandler(TokenTimeOutException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ErrorMessage handleTokenTimeOutException(TokenTimeOutException ex) {
        ErrorMessage errorMessage = new ErrorMessage(LocalDate.now(), 401, "Refresh Token has been expired. Please login again!");
        LOGGER.error(errorMessage.toString());
        return errorMessage;
    }

    @ExceptionHandler(TokenNotFoundException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage handleTokenNotFoundException(TokenNotFoundException ex) {
        ErrorMessage errorMessage = new ErrorMessage(LocalDate.now(), 400, "Oops! Can't find the refresh token. Please try the other one or ⁈login to get new token⁈");
        LOGGER.error(errorMessage.toString());
        return errorMessage;
    }

}
