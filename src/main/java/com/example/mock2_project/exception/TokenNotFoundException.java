package com.example.mock2_project.exception;

import java.io.Serializable;

public class TokenNotFoundException extends Exception implements Serializable {
    static final long serialVersionUID = -7034897190745766939L;
    public TokenNotFoundException(String message) {
        super(message);
    }
}
