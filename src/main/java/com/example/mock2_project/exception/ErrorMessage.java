package com.example.mock2_project.exception;

import lombok.*;

import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ErrorMessage {
    private LocalDate dateError;
    private Integer statusCode;
    private String message;
}