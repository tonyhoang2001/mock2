package com.example.mock2_project.entity;

import com.example.mock2_project.enums.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//nguồn tham khảo Spring Validation: https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#section-builtin-constraints

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "account")
public class Account extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Long id;

    @Email(message = "wrong Email format: a@b", regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")
    //@NotEmpty(message = "Email không được bỏ trống")@Column(name = "account_email")
    private String email;

    @Column(name = "account_password")
    //khong the dat regex vi password dang bi encrypt BCrypt nen dinh dang se khac
    //@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$", message = "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character")
    private String password;

    //@Pattern(regexp = "ADMIN|USER", message = "Invalid role! Input ADMIN/USER")
    @Enumerated(EnumType.STRING)
    @Column(name = "account_role")
    private Role role;

    @Column(name = "account_name")
    @NotBlank(message = "Need to fill the name")
    private String fullName;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    private FileDB fileDB;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnore
    private List<Rating> ratings = new ArrayList<>();

    @OneToMany(mappedBy = "account", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Invoice> invoices = new ArrayList<>();

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    private AccountInfo accountInfo;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    @JoinColumn(name = "refresh_token_id")
    private RefreshToken refreshToken;

    @OneToMany(mappedBy = "account", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<AccessToken> accessTokens;

    public Account(String email, String encode, Role role, String fullName) {
    }

    public Account(LocalDateTime createdAt, String createdBy, LocalDateTime updatedAt, String updatedBy, String email, String passwordEncoded, String fullName, Role role, FileDB fileDB, AccountInfo accountInfo, List<AccessToken> accessTokens, RefreshToken refreshToken) {
        super(createdAt, updatedAt, createdBy, updatedBy);
        this.email = email;
        this.password = passwordEncoded;
        this.role = role;
        this.fullName = fullName;
        this.fileDB = fileDB;
        this.accountInfo = accountInfo;
        this.accessTokens = accessTokens;
        this.refreshToken = refreshToken;
    }
}
