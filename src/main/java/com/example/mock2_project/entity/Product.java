package com.example.mock2_project.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "product")
public class Product extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    @Min(value = 1, message = "Invalid ID!")
    private Long id;
    @Column(name = "product_name")
    private String name;
    @Column(name = "product_description")
    private String description;
    @Column(name = "product_price")
    @Min(value = 0, message = "Invalid price!")
    private double price;
    @Column(name = "product_quantity")
    @Min(value = 0, message = "Invalid quantity!")
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Rating> ratings = new ArrayList<>();;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private InvoiceDetail invoiceDetail;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "file_id")
    private FileDB fileDB;

    public Product(LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, String name, String description, double price, int quantity, Category category, FileDB fileDB) {
        super(createdAt, updatedAt, createdBy, updatedBy);
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
        this.fileDB = fileDB;
    }

    public Product(String name, String description, double price, int quantity, Category category, FileDB fileDB) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.category = category;
        this.fileDB = fileDB;
    }
}
