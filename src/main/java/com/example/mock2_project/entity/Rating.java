package com.example.mock2_project.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "rating")
public class Rating extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_id")
    private Long id;
    private String comment;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public Rating(String comment, Account account, Product product) {
        this.comment = comment;
        this.account = account;
        this.product = product;
    }

    public Rating(LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, String comment, Account account, Product product) {
        super(createdAt, updatedAt, createdBy, updatedBy);
        this.comment = comment;
        this.account = account;
        this.product = product;
    }
}
