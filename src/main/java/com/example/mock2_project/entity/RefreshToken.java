package com.example.mock2_project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "refresh_token")
public class RefreshToken extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "refresh_token_id")
    private Long id;
    @Column(name = "refresh_token")
    private String refreshToken;
    @Column(name = "expired_date")
    private LocalDateTime expiredDate;

    @OneToOne
    @JoinColumn(name = "account_id")
    //dùng annotation JsonIgnore để ẩn hết thông tin của bảng reference (account)
    @JsonIgnore
    private Account account;
}
