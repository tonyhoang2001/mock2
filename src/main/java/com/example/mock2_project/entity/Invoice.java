package com.example.mock2_project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "invoice")
public class Invoice extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;
    @Column(name = "total_price")
    private double total;
    @Column(name = "expected_date")
    private LocalDate expectedDate;
    @Column(name = "shipped_date")
    private LocalDate shippedDate;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @JsonIgnore
    private Account account;

    @OneToMany(mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<InvoiceDetail> invoiceDetails = new ArrayList<>();;

    public Invoice(LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, double total, LocalDate expectedDate, LocalDate shippedDate, Account account) {
        super(createdAt, updatedAt, createdBy, updatedBy);
        this.total = total;
        this.expectedDate = expectedDate;
        this.shippedDate = shippedDate;
        this.account = account;
    }
}
