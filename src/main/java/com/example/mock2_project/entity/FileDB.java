package com.example.mock2_project.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "file_db")
public class FileDB extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private Long id;
    private String name;
    private String type;
    @Lob
    @Column(length = 1000000000)
    private byte[] data;

    @OneToOne(mappedBy = "fileDB", cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Product product;

    @OneToOne
    @JoinColumn(name = "account_id")
    @JsonIgnore
    private Account account;

    public FileDB(String name, String type, byte[] data) {
        this.name = name;
        this.type = type;
        this.data = data;
    }

    public FileDB(Long id, String name, String type, byte[] data) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.data = data;
    }

    public FileDB(LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, String name, String type, byte[] data) {
        super(createdAt, updatedAt, createdBy, updatedBy);
        this.name = name;
        this.type = type;
        this.data = data;
    }

    public FileDB(LocalDateTime updatedAt, String updatedBy,Long id, String name, String type, byte[] data) {
        super(updatedAt,updatedBy);
        this.id = id;
        this.name = name;
        this.type = type;
        this.data = data;
    }
}
