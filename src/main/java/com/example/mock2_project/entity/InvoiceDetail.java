package com.example.mock2_project.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "invoice_detail")
public class InvoiceDetail extends AbstractEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "invoice_detail_id")
    private Long id;
    @Column(name = "product_quantity")
    private int quantity;
    @Column(name = "total_price")
    private double totalPrice;

    @ManyToOne
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public InvoiceDetail(LocalDateTime createdAt, LocalDateTime updatedAt, String createdBy, String updatedBy, int quantity, double totalPrice, Invoice invoice, Product product) {
        super(createdAt, updatedAt, createdBy, updatedBy);
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.invoice = invoice;
        this.product = product;
    }

    public InvoiceDetail(int quantity, double totalPrice, Invoice invoice, Product product) {
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.invoice = invoice;
        this.product = product;
    }
}
