package com.example.mock2_project.entity;

import com.example.mock2_project.enums.AccessTokenType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "access_token")
public class AccessToken extends AbstractEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "access_token_id")
    private Long id;

    @Column(name = "access_token")
    private String accessToken;

    @Column(name = "access_token_type")
    @Enumerated(EnumType.STRING)
    private AccessTokenType accessTokenType;

    //Boolean variables should be prefixed with ‘is’: e.g. isSet, isVisible, isFinished, isFound, isOpen
    @Column(name = "is_expired")
    private boolean isExpired;

    //Boolean variables should be prefixed with ‘is’: e.g. isSet, isVisible, isFinished, isFound, isOpen
    @Column(name = "is_revoked")
    private boolean isRevoked;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    //dùng annotation JsonIgnore để ẩn hết thông tin của bảng reference (if needed)
    @JsonIgnore
    private Account account;
}
