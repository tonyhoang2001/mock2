package com.example.mock2_project.jwt;

import com.example.mock2_project.config.UserDetailsImpl;
import com.example.mock2_project.exception.TokenTimeOutException;
import com.example.mock2_project.repository.AccessTokenRepository;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.service.AccountServiceImpl;
import com.example.mock2_project.service.UserDetailsServiceImpl;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;


@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    public JwtAuthenticationFilter() {
    }

    @SneakyThrows
    @Override
    protected void doFilterInternal (
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        final String authHeader = request.getHeader("Authorization");
        final String jwt;
        final String userEmail;

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
            return;
        }

        jwt = authHeader.substring(7);
        userEmail = jwtProvider.extractUsername(jwt);

        if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            UserDetailsImpl userDetails = userDetailsService.loadUserByUsername(userEmail);

            boolean isAccessTokenAvailable = accessTokenRepository.findByAccessToken(jwt)
                    .map(token -> !token.isExpired() && !token.isRevoked())
                    .orElse(false);

            if (jwtProvider.isTokenValid(jwt, userDetails) && isAccessTokenAvailable) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails, //principle
                        null, //credentials
                        userDetails.getAuthorities()); //authorities
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            } else {
                throw new TokenTimeOutException("access token is revoked or expired");
            }
        }
        filterChain.doFilter(request, response);
    }
}
