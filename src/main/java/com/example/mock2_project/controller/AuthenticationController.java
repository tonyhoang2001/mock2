package com.example.mock2_project.controller;


import com.example.mock2_project.dto.request.LoginRequest;
import com.example.mock2_project.dto.request.RegisterRequest;
import com.example.mock2_project.dto.request.ReLoginRequest;
import com.example.mock2_project.dto.response.LoginResponse;
import com.example.mock2_project.dto.response.ReLoginResponse;
import com.example.mock2_project.dto.response.RegisterResponse;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.exception.TokenNotFoundException;
import com.example.mock2_project.exception.TokenTimeOutException;
import com.example.mock2_project.service.AccountServiceImpl;
import com.example.mock2_project.service.AuthenticationServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationServiceImpl authenticationServiceImpl;

    @PostMapping("register")
    public ResponseEntity<RegisterResponse> accountRegister(MultipartFile file, @RequestBody @Valid RegisterRequest registerRequest, HttpServletRequest request) throws Exception {
        System.out.println("register api");
        return ResponseEntity.ok(authenticationServiceImpl.serviceRegister(file, registerRequest, request));
    }

    @PostMapping("login")
    public ResponseEntity<LoginResponse> accountLogin(@RequestBody LoginRequest loginRequest) throws TokenTimeOutException, TokenNotFoundException {
        return ResponseEntity.ok(authenticationServiceImpl.serviceLogin(loginRequest));
    }

    @PostMapping("re-login")
    public ResponseEntity<ReLoginResponse> accountReLogin(@RequestBody ReLoginRequest reLoginRequest) throws TokenTimeOutException, TokenNotFoundException {
        return ResponseEntity.ok(authenticationServiceImpl.serviceReLogin(reLoginRequest));
    }
}
