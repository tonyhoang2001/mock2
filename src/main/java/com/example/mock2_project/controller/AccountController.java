package com.example.mock2_project.controller;

import com.example.mock2_project.dto.response.AccountResponse;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.service.AccountServiceImpl;
import com.example.mock2_project.service.FileDBService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/account")
@Slf4j
public class AccountController {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    private AccountServiceImpl accountServiceImpl;

    @Autowired
    private FileDBService fileDBService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<Account> getAllTheRegister() {
        return accountRepository.findAll();
    }
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AccountResponse>> getAllAccount(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(value = 1, message = "Page is invalid!") Integer page,
                                                               @RequestParam(value = "size", required = false, defaultValue = "10") @Min(value = 1, message = "Size is invalid!") Integer size) {
        log.info("user get all account {}", accountServiceImpl.getAllAccount(page, size));
        return ResponseEntity.ok().body(accountServiceImpl.getAllAccount(page, size));
    }

    @GetMapping("/search")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AccountResponse> getAccountById(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(value = 1, message = "Page is invalid!") Integer page,
                                                          @RequestParam(value = "size", required = false, defaultValue = "10") @Min(value = 1, message = "Size is invalid!") Integer size,
                                                          @RequestParam(value = "id", required = false, defaultValue = "1") @Min(value = 1, message = "ID is invalid!") long id) {
        return ResponseEntity.ok().body(accountServiceImpl.getAccountById(id, page, size));
    }

    @GetMapping("/search-email")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AccountResponse> getAccountByEmail(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(value = 1, message = "Page is invalid!") Integer page,
                                                             @RequestParam(value = "size", required = false, defaultValue = "10") @Min(value = 1, message = "Size is invalid!") Integer size,
                                                             @RequestParam(value = "email", required = false, defaultValue = "1") String email) {
        return ResponseEntity.ok().body((AccountResponse) accountServiceImpl.getAccountByUsername(email, page, size));
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<AccountResponse> addAccount(@RequestParam(value = "file") MultipartFile file,
                                                      @RequestParam(value = "account") String accountJson,
                                                      HttpServletRequest request) {
        return ResponseEntity.ok().body(accountServiceImpl.addAccount(file, accountJson, request));
    }


    @PutMapping("/update")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<AccountResponse> updateAccount(@RequestParam(value = "file") @NotNull MultipartFile file,
                                                         @RequestParam(value = "account") String accountJson,
                                                         HttpServletRequest request) {
        return ResponseEntity.ok().body(accountServiceImpl.updateAccount(file, accountJson, request));
    }

    @DeleteMapping("delete/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteAccount(@PathVariable @Min(value = 1, message = "ID is invalid!") long id) {
        log.error("cant {}", id);
        accountServiceImpl.deleteAccount(id);
        return ResponseEntity.ok().body("Delete account successful!");
    }

    @GetMapping("/file/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<byte[]> getFile(@PathVariable @Min(value = 1, message = "ID is invalid!") long id) {
        FileDB fileDB = fileDBService.getFileById(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }
}
