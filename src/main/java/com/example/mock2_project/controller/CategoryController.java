package com.example.mock2_project.controller;

import com.example.mock2_project.dto.request.CategoryRequest;
import com.example.mock2_project.dto.response.CategoryResponse;
import com.example.mock2_project.message.ResponseMessage;
import com.example.mock2_project.service.CategoryService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
@Validated
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> getAllCategories(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(1) Integer page,
                                                            @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size) {
        List<CategoryResponse> categories = categoryService.getAllCategories(page, size);
        return new ResponseEntity<>
                (categories != null ?
                        new ResponseMessage<>(categories, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no category!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> getCategoryById(@PathVariable @Min(value = 1, message = "ID is invalid!") Long id) {
        CategoryResponse categoryResponse = categoryService.getCategoryById(id);
        return new ResponseEntity<>(
                categoryResponse != null ?
                        new ResponseMessage(categoryResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage("There's no category!", HttpStatus.OK.value(), "Successful!"),
                HttpStatus.OK
        );
    }

    @PostMapping("")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> addCategory(@RequestBody CategoryRequest category,
                                                       HttpServletRequest request) {
        CategoryResponse categoryResponse = categoryService.addCategory(category, request);
        return new ResponseEntity<>(
                categoryResponse != null ?
                        new ResponseMessage(categoryResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage("Add fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!"),
                HttpStatus.OK
        );
    }

    @PutMapping("")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> updateCategory(@RequestBody CategoryRequest category,
                                                          HttpServletRequest request) {
        CategoryResponse categoryResponse = categoryService.updateCategory(category, request);
        return new ResponseEntity<>(
                categoryResponse != null ?
                        new ResponseMessage(categoryResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage("Update fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!"),
                HttpStatus.OK
        );
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> deleteCategory(@PathVariable @Valid @Min(value = 1, message = "ID is invalid!") long id) {
        boolean b = categoryService.deleteCategory(id);
        return new ResponseEntity<>(b ?
                new ResponseMessage("Delete successful!", HttpStatus.OK.value(), "Successful!") :
                new ResponseMessage("Delete fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!"),
                HttpStatus.OK
        );
    }
}
