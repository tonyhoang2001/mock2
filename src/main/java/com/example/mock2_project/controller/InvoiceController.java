package com.example.mock2_project.controller;

import com.example.mock2_project.dto.request.InvoiceRequest;
import com.example.mock2_project.dto.response.InvoiceResponse;
import com.example.mock2_project.message.ResponseMessage;
import com.example.mock2_project.service.InvoiceService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/invoice")
@Validated
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping
    public ResponseEntity<ResponseMessage> getAllByAccount(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(1) Integer page,
                                                           @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size,
                                                           @RequestParam(value = "accountId") @Min(value = 1, message = "ID is invalid!") Long accountId) {
        List<InvoiceResponse> invoices = invoiceService.getAllByAccount(page, size, accountId);
        return new ResponseEntity<>
                (invoices != null ?
                        new ResponseMessage<>(invoices, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no invoice!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseMessage> getById(@PathVariable @Min(value = 1, message = "ID is invalid!") Long id) {
        InvoiceResponse invoice = invoiceService.getById(id);
        return new ResponseEntity<>
                (invoice != null ?
                        new ResponseMessage<>(invoice, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no invoice!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @PostMapping
    public ResponseEntity<ResponseMessage> addInvoice(@RequestBody @Valid InvoiceRequest invoiceRequest,
                                                      HttpServletRequest request) {
        InvoiceResponse invoiceResponse = invoiceService.addInvoice(invoiceRequest, request);
        return new ResponseEntity<>
                (invoiceResponse != null ?
                        new ResponseMessage<>(invoiceResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Add fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @PutMapping
    public ResponseEntity<ResponseMessage> updateInvoice(@RequestBody @Valid InvoiceRequest invoiceRequest,
                                                         HttpServletRequest request) {
        InvoiceResponse invoiceResponse = invoiceService.updateInvoice(invoiceRequest, request);
        return new ResponseEntity<>
                (invoiceResponse != null ?
                        new ResponseMessage<>(invoiceResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Update fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMessage> deleteInvoice(@PathVariable @Min(value = 1, message = "ID is invalid!") Long id) {
        boolean b = invoiceService.deleteInvoice(id);
        return new ResponseEntity<>
                (b ? new ResponseMessage<>("Delete successful!", HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Delete fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }


}
