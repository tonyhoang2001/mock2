package com.example.mock2_project.controller;

import com.example.mock2_project.entity.AccountInfo;
import com.example.mock2_project.service.AccountInfoServiceImpl;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/account-info")
public class AccountInfoController {
    @Autowired
    private AccountInfoServiceImpl accountInfoServiceImpl;

    @GetMapping("list")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<List<AccountInfo>> getAllAccountInfos(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(value = 1, message = "Page is invalid!") Integer page,
                                                                @RequestParam(value = "size", required = false, defaultValue = "10") @Min(value = 1, message = "Size is invalid!") Integer size) throws Exception {
        List<AccountInfo> accountInfos = accountInfoServiceImpl.getAllAccountInfos(page, size);
        return ResponseEntity.ok(accountInfos);
    }

    @GetMapping("getbyeid/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<AccountInfo> getAccountInfoById(@PathVariable @Min(value = 1, message = "ID is invalid!") long id) {
        return ResponseEntity.ok(accountInfoServiceImpl.getAccountInfoById(id));
    }

    @GetMapping("getbyemail")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<AccountInfo> getAccountInfoByAccountEmail(@RequestParam("email") String email) {
        return ResponseEntity.ok(accountInfoServiceImpl.getAccountInfoByAccountEmail(email));
    }

    @PutMapping("update")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<AccountInfo> updateAccountInfoByAccountEmail(@RequestBody AccountInfo accountInfo) {
        log.info("{}", accountInfo);
        return ResponseEntity.ok(accountInfoServiceImpl.updateAccountInfoByAccountEmail(accountInfo));
    }

    @DeleteMapping("delete/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteAccountInfoById(@PathVariable @Valid @Min(value = 1, message = "ID is invalid!") long id) {
        accountInfoServiceImpl.deleteAccountInfoById(id);
        return ResponseEntity.ok().body("Delete account information successful!");
    }

    @DeleteMapping("delete")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<?> deleteAccountInfoByAccountEmail(@RequestParam("email") String email) {
        accountInfoServiceImpl.deleteAccountInfoByAccountEmail(email);
        return ResponseEntity.ok().body("Delete account information successful!");
    }
}
