package com.example.mock2_project.controller;

import com.example.mock2_project.dto.response.ProductResponse;
import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.message.ResponseMessage;
import com.example.mock2_project.service.FileDBService;
import com.example.mock2_project.service.ProductService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/product")
@Validated
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private FileDBService fileDBService;

    @GetMapping()
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> getAllProducts(
            @RequestParam(value = "page", required = false, defaultValue = "1") @Min(1) Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size) {
        List<ProductResponse> products = productService.getAllProducts(page, size);
        return new ResponseEntity<>
                (products != null ?
                        new ResponseMessage<>(products, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no product!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @GetMapping("/search")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> getProductById(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(1) Integer page,
                                                          @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size,
                                                          @RequestParam(value = "id") @Min(value = 1, message = "ID is invalid!") long id) {
        ProductResponse product = productService.getProductById(id, page, size);
        return new ResponseEntity<>
                (product != null ?
                        new ResponseMessage<>(product, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no product!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @PostMapping()
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> addProduct(@RequestParam(value = "file") MultipartFile file,
                                                      @RequestParam(value = "product") String productJson,
                                                      HttpServletRequest request) {
        ProductResponse product = productService.addProduct(file, productJson, request);
        return new ResponseEntity<>
                (product != null ?
                        new ResponseMessage<>(product, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Add fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @PutMapping()
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> updateProduct(@RequestParam(value = "file") MultipartFile file,
                                                         @RequestParam(value = "product") String productJson,
                                                         HttpServletRequest request) {
        ProductResponse product = productService.updateProduct(file, productJson, request);
        return new ResponseEntity<>
                (product != null ?
                        new ResponseMessage<>(product, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Update fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> deleteProduct(@PathVariable @Min(value = 1, message = "ID is invalid!") long id) {
        boolean b = productService.deleteProduct(id);
        return new ResponseEntity<>
                (b ?
                        new ResponseMessage<>("Delete successful!", HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Update fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @GetMapping("/file/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable @Min(value = 1, message = "ID is invalid!") long id) {
        FileDB fileDB = fileDBService.getFileById(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
//                .contentType(MediaType.valueOf(fileDB.getType()))
                .body(fileDB.getData());
    }

}
