package com.example.mock2_project.controller;

import com.example.mock2_project.dto.request.InvoiceDetailRequest;
import com.example.mock2_project.dto.response.InvoiceDetailResponse;
import com.example.mock2_project.message.ResponseMessage;
import com.example.mock2_project.service.InvoiceDetailService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/invoice-detail")
@Validated
public class InvoiceDetailController {

    @Autowired
    private InvoiceDetailService invoiceDetailService;

    @GetMapping("")
    public ResponseEntity<ResponseMessage> getByInvoiceId(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(1) Integer page,
                                                          @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size,
                                                          @RequestParam(value = "invoiceId") @Min(value = 1, message = "ID is invalid!") Long invoiceId) {
        List<InvoiceDetailResponse> invoiceDetails = invoiceDetailService.getByInvoiceId(page, size, invoiceId);
        return new ResponseEntity<>
                (invoiceDetails != null ?
                        new ResponseMessage<>(invoiceDetails, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no invoice detail!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseMessage> getById(@PathVariable @Min(value = 1, message = "ID is invalid!") Long id) {
        InvoiceDetailResponse invoiceDetail = invoiceDetailService.getById(id);
        return new ResponseEntity<>
                (invoiceDetail != null ?
                        new ResponseMessage<>(invoiceDetail, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no invoice detail!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @PutMapping
    public ResponseEntity<ResponseMessage> update(@RequestBody @Valid InvoiceDetailRequest detailRequest,
                                                  HttpServletRequest request) {
        InvoiceDetailResponse invoiceDetail = invoiceDetailService.update(detailRequest, request);
        return new ResponseEntity<>
                (invoiceDetail != null ?
                        new ResponseMessage<>(invoiceDetail, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Update fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMessage> delete(@PathVariable @Min(value = 1, message = "Size is invalid!") Long id) {
        boolean b = invoiceDetailService.delete(id);
        return new ResponseEntity<>
                (b ? new ResponseMessage<>("Delete successful!", HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Delete fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

}
