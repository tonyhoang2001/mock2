package com.example.mock2_project.controller;

import com.example.mock2_project.dto.request.AddingRatingRequest;
import com.example.mock2_project.dto.request.UpdatingRatingRequest;
import com.example.mock2_project.dto.response.RatingResponse;
import com.example.mock2_project.message.ResponseMessage;
import com.example.mock2_project.service.RatingService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rating")
@Validated
public class RatingController {

    @Autowired
    private RatingService ratingService;

    @GetMapping("/search/product")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> getRatingByProduct(@RequestParam(value = "page", required = false, defaultValue = "1") @Min(1) Integer page,
                                                              @RequestParam(value = "size", required = false, defaultValue = "10") @Min(1) Integer size,
                                                              @RequestParam(value = "productId") @Min(value = 1, message = "ID is invalid") Long productId) {
        List<RatingResponse> ratings = ratingService.getRatingsByProduct(page, size, productId);
        return new ResponseEntity<>
                (ratings != null ?
                        new ResponseMessage<>(ratings, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("There's no rating!", HttpStatus.OK.value(), "Successful!"),
                        HttpStatus.OK
                );
    }

    @PostMapping("")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> addRating(@RequestBody @Valid AddingRatingRequest ratingRequest, HttpServletRequest request) {
        RatingResponse ratingResponse = ratingService.addRating(ratingRequest, request);
        return new ResponseEntity<>
                (ratingResponse != null ?
                        new ResponseMessage<>(ratingResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Add fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @PutMapping("")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> updateRating(@RequestBody @Valid UpdatingRatingRequest ratingRequest, HttpServletRequest request) {
        RatingResponse ratingResponse = ratingService.updateRating(ratingRequest, request);
        return new ResponseEntity<>
                (ratingResponse != null ?
                        new ResponseMessage<>(ratingResponse, HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Update fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<ResponseMessage> deleteRating(@PathVariable @Min(value = 1, message = "ID is invalid") Long id) {
        boolean b = ratingService.deleteRating(id);
        return new ResponseEntity<>
                (b ?
                        new ResponseMessage<>("Delete successful!", HttpStatus.OK.value(), "Successful!") :
                        new ResponseMessage<>("Delete fail!", HttpStatus.METHOD_FAILURE.value(), "Fail!!"),
                        HttpStatus.OK
                );
    }

}
