package com.example.mock2_project.service;

import com.example.mock2_project.dto.request.AddingProductRequest;
import com.example.mock2_project.dto.request.UpdatingProductRequest;
import com.example.mock2_project.dto.response.CategoryResponse;
import com.example.mock2_project.dto.response.ProductResponse;
import com.example.mock2_project.dto.response.RatingResponse;
import com.example.mock2_project.dto.response.ResponseFile;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.Category;
import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.entity.Product;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.CategoryRepository;
import com.example.mock2_project.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private FileDBService fileDBService;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private JwtProvider jwtProvider;

    public List<ProductResponse> getAllProducts(Integer page, Integer size) {
        LOGGER.info("CALLING: GET ALL PRODUCTS API");
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Product> products = productRepository.findAll(pageable);

        if (products.getContent().isEmpty()) {
            LOGGER.warn("Product list is empty!");
            return null;
        }

        LOGGER.info("Got product list!");
        return products.stream().map(
                p -> {
                    Category category = p.getCategory();
                    ResponseFile responseFile = null;

                    if (p.getFileDB() != null) {
                        FileDB fileDB = fileDBService.getFileById(p.getFileDB().getId());

                        if (fileDB != null) {
                            String fileDownloadUri = "http://localhost:8080/product/file/" + fileDB.getId();

                            responseFile = new ResponseFile(
                                    fileDB.getName(),
                                    fileDownloadUri.toString(),
                                    fileDB.getType(),
                                    fileDB.getData().length);
                        }
                    }

                    return new ProductResponse(p.getId(), p.getName(), p.getDescription(), p.getPrice(), p.getQuantity(),
                            new CategoryResponse(category.getId(), category.getCategoryName()),
                            responseFile, null);

                }
        ).collect(Collectors.toList());
    }

    public ProductResponse getProductById(long id, Integer page, Integer size) {
        LOGGER.info("CALLING: GET PRODUCT BY ID API");
        return productRepository.findById(id).map(
                        p -> {
                            Category category = p.getCategory();
                            ResponseFile responseFile = null;

                            if (p.getFileDB() != null) {
                                FileDB fileDB = fileDBService.getFileById(p.getFileDB().getId());

                                if (fileDB != null) {
                                    String fileDownloadUri = "http://localhost:8080/product/file/" + fileDB.getId().toString();

                                    responseFile = new ResponseFile(
                                            fileDB.getName(),
                                            fileDownloadUri.toString(),
                                            fileDB.getType(),
                                            fileDB.getData().length);
                                }
                            }

                            List<RatingResponse> ratingResponses = ratingService.getRatingsByProduct(page, size, p.getId());

                            return new ProductResponse(p.getId(), p.getName(), p.getDescription(), p.getPrice(), p.getQuantity(),
                                    new CategoryResponse(category.getId(), category.getCategoryName()),
                                    responseFile, ratingResponses);
                        }
                )
                .orElseGet(null);
    }

    public ProductResponse addProduct(MultipartFile file, String productRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: ADD PRODUCT API");
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            AddingProductRequest addingProductRequest = objectMapper.readValue(productRequest, AddingProductRequest.class);

            Category category = categoryRepository.findById(addingProductRequest.getCategoryId()).orElseThrow(() -> {
                LOGGER.error("Not found category!");
                throw new RuntimeException("No category found!");
            });

            FileDB fileDB = fileDBService.saveFile(file, request);
            Account account = jwtProvider.getAccountFromToken(request);

            Product product = productRepository.save(new Product(LocalDateTime.now(), null, account.getEmail(), null, addingProductRequest.getName(), addingProductRequest.getDescription(), addingProductRequest.getPrice(),
                    addingProductRequest.getQuantity(), category, fileDB));

            ResponseFile responseFile = null;

            if (fileDB != null) {
                String fileDownloadUri = "http://localhost:8080/product/file/" + fileDB.getId().toString();

                responseFile = new ResponseFile(
                        fileDB.getName(),
                        fileDownloadUri.toString(),
                        fileDB.getType(),
                        fileDB.getData().length);
            }

            ProductResponse productResponse = new ProductResponse(product.getId(), product.getName(), product.getDescription(), product.getPrice(), product.getQuantity(),
                    new CategoryResponse(category.getId(), category.getCategoryName()), responseFile, null);
            LOGGER.info("Add product successful!");
            return productResponse;
        } catch (Exception e) {
            LOGGER.error("Add product fail!");
            return null;
        }
    }

    public ProductResponse updateProduct(MultipartFile file, String productRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: UPDATE PRODUCT API");
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            UpdatingProductRequest updatingProductRequest = objectMapper.readValue(productRequest, UpdatingProductRequest.class);

            Product product = productRepository.findById(updatingProductRequest.getId()).orElseThrow(() -> {
                LOGGER.error("No product found");
                throw new RuntimeException("No product found!");
            });

            Category category = categoryRepository.findById(updatingProductRequest.getCategoryId()).orElseThrow(() -> {
                LOGGER.error("No category found");
                throw new RuntimeException("No category found!");
            });

            FileDB fileDB;
            if (product.getFileDB() != null) {
                fileDB = fileDBService.updateFile(file, product.getFileDB().getId(), request);
            } else {
                fileDB = fileDBService.saveFile(file, request);
            }
            Account account = jwtProvider.getAccountFromToken(request);

            product.setFileDB(fileDB);
            product.setName(updatingProductRequest.getName());
            product.setDescription(updatingProductRequest.getDescription());
            product.setPrice(updatingProductRequest.getPrice());
            product.setQuantity(updatingProductRequest.getQuantity());
            product.setCategory(category);
            product.setUpdatedAt(LocalDateTime.now());
            product.setUpdatedBy(account.getEmail());
            productRepository.save(product);

            ResponseFile responseFile = null;

            if (fileDB != null) {
                String fileDownloadUri = "http://localhost:8080/product/file/" + fileDB.getId();

                responseFile = new ResponseFile(
                        fileDB.getName(),
                        fileDownloadUri.toString(),
                        fileDB.getType(),
                        fileDB.getData().length);
            }

            ProductResponse productResponse = new ProductResponse(product.getId(), product.getName(), product.getDescription(), product.getPrice(), product.getQuantity(),
                    new CategoryResponse(category.getId(), category.getCategoryName()), responseFile, null);
            LOGGER.info("Update product successful!");
            return productResponse;
        } catch (Exception e) {
            LOGGER.error("Update product fail!");
            return null;
        }
    }

    public boolean deleteProduct(Long id) {
        LOGGER.info("CALLING: DELETE PRODUCT API");
        Product product = productRepository.findById(id).orElseThrow(() -> {
            LOGGER.error("No product found!");
            throw new RuntimeException("Not found product!");
        });

        try {
            List<RatingResponse> ratingResponses = ratingService.getRatingsByProduct(0, 0, product.getId());
            if (ratingResponses != null) {
                for (RatingResponse ratingResponse : ratingResponses) {
                    ratingService.deleteRating(ratingResponse.getId());
                }
            }


            FileDB fileDB = fileDBService.getFileById(product.getFileDB().getId());
            if (fileDB != null) {
                fileDBService.deleteFileById(fileDB);
            }
            productRepository.delete(product);
            LOGGER.info("Delete product successful!");
            return true;
        } catch (Exception e) {
            LOGGER.info("Delete product fail!");
            return false;
        }
    }

}
