package com.example.mock2_project.service;

import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.FileDB;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.FIleDBRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.stream.Stream;

@Service
@Transactional
public class FileDBService {

    @Autowired
    private FIleDBRepository fIleDBRepository;

    @Autowired
    private JwtProvider jwtProvider;

    public FileDB saveFile(MultipartFile file, HttpServletRequest request) {
        FileDB savedFile = null;
        try {
            String fileName = file.getOriginalFilename();
            if (!"".equals(fileName)) {
                Account account = jwtProvider.getAccountFromToken(request);
                FileDB fileDB = new FileDB(LocalDateTime.now(), null, account.getEmail(), null, fileName, file.getContentType(), file.getBytes());
                savedFile = fIleDBRepository.save(fileDB);
            }
        } catch (Exception e) {
            throw new RuntimeException("Save image file is not successful!");
        }

        return savedFile;
    }

    public FileDB updateFile(MultipartFile file, Long fileId, HttpServletRequest request) {
        FileDB savedFile;
        try {
            String fileName = file.getOriginalFilename();
            Account account = jwtProvider.getAccountFromToken(request);
            FileDB fileDB = new FileDB(LocalDateTime.now(), account.getEmail(), fileId, fileName, file.getContentType(), file.getBytes());
            savedFile = fIleDBRepository.save(fileDB);
        } catch (Exception e) {
            throw new RuntimeException("Save image file is not successful!");
        }

        return savedFile;
    }

    public FileDB getFileById(Long id) {
        return fIleDBRepository.findById(id).orElseThrow(() -> {
            return null;
//            throw new EntityNotFoundException("Not found file with id: " + id);
        });
    }

    public FileDB getFileDBByAccountId(Long id) {
        return fIleDBRepository.findByAccountId(id);
    }

    public Stream<FileDB> getAllFiles() {
        return fIleDBRepository.findAll().stream();
    }

    public void deleteFileById(FileDB fileDB) {
        try {
            fIleDBRepository.delete(fileDB);
        } catch (Exception e) {
            throw new RuntimeException("Delete file fail!");
        }
    }

    public void deleteFileDBById(Long id) {
        try {
            fIleDBRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Delete file fail!");
        }
    }
}
