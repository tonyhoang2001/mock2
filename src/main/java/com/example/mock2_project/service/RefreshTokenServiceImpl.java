package com.example.mock2_project.service;

import com.example.mock2_project.entity.RefreshToken;
import com.example.mock2_project.exception.TokenNotFoundException;
import com.example.mock2_project.exception.TokenTimeOutException;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.repository.RefreshTokenRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

@Service
@Transactional
public class RefreshTokenServiceImpl {
    private long REFRESH_TOKEN_EXPIRATION = 60 * 60 * 12 ; //12hours

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private AccountRepository accountRepository;

    public RefreshToken findByToken(String token) throws TokenNotFoundException {
        return refreshTokenRepository.findByRefreshToken(token).orElseThrow(() -> new TokenNotFoundException("Refresh token " + token + " is not exist!"));
    }

    public boolean checkRefreshTokenExistByCreator(String email) {
        return refreshTokenRepository.findRefreshTokenByCreatedBy(email).isPresent();
    }

    public RefreshToken findRefreshTokenByCreator(String email) throws TokenNotFoundException {
        return refreshTokenRepository.findRefreshTokenByCreatedBy(email).orElseThrow(()->new TokenNotFoundException("Not found any token related to the email: " + email));
    }

    public RefreshToken createRefreshTokenByUsername(String username) {
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setAccount(accountRepository.findByEmail(username).orElseThrow());
        refreshToken.setExpiredDate(LocalDateTime.now().plusSeconds(REFRESH_TOKEN_EXPIRATION));
        refreshToken.setRefreshToken(UUID.randomUUID().toString());
        refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public boolean isRefreshTokenExpired(RefreshToken refreshToken) throws TokenTimeOutException {
        System.out.println(refreshToken.getExpiredDate().isBefore(LocalDateTime.now()));
        if (refreshToken.getExpiredDate().isBefore(LocalDateTime.now())) {
            refreshTokenRepository.delete(refreshToken);
            throw new TokenTimeOutException("Out of time. Refresh token has been expired!");
        }
        return refreshToken.getExpiredDate().isBefore(LocalDateTime.now());
    }

    public int deleteRefreshTokenByAccountId(Long accountId) {
        return refreshTokenRepository.deleteRefreshTokenByAccount(accountRepository.findById(accountId).get());
    }

    public void deleteTokenByEmail(String email) {
        refreshTokenRepository.deleteRefreshTokenByCreatedBy(email);
    }

    public String getRemainingTime(RefreshToken token) {
        ZonedDateTime zonedDateTimeOfExpiredDate = token.getExpiredDate().atZone(ZoneId.of("Asia/Ho_Chi_Minh"));
        long expiredDateMillis = zonedDateTimeOfExpiredDate.toInstant().toEpochMilli();
        long longTotalRemainSeconds = (expiredDateMillis - System.currentTimeMillis()) / 1000;

        long longSeconds = longTotalRemainSeconds % 60;
        long longTotalMinutes = longTotalRemainSeconds / 60;
        long longMinutes = longTotalMinutes % 60;
        long longHours = longTotalMinutes / 60;

        String stringRemainSeconds = String.valueOf(longSeconds);
        String stringRemainMinutes = String.valueOf(longMinutes);
        String stringRemainHours = String.valueOf(longHours);

        String stringTotalRemainSeconds = String.valueOf(longTotalRemainSeconds);
        //String remainingTime = "Your current refresh token will expired in next: " + stringTotalRemainSeconds + "s " + "(" + stringRemainHours + "h:" + stringRemainMinutes + "m:" + stringRemainSeconds + "s)";
        String remainingTime = stringTotalRemainSeconds + "s " + "(" + stringRemainHours + "h:" + stringRemainMinutes + "m:" + stringRemainSeconds + "s)";
        return remainingTime;
    }

    public String getDuration() {
        //long longTotalSeconds = 12 * 60 * 60;
        long longTotalSeconds = REFRESH_TOKEN_EXPIRATION;

        long longSeconds = longTotalSeconds % 60;
        long longTotalMinute = longTotalSeconds / 60;
        long longMinutes = longTotalMinute % 60;
        long longHours = longTotalMinute / 60;

        String stringSeconds = String.valueOf(longSeconds);
        String stringMinutes = String.valueOf(longMinutes);
        String stringHours = String.valueOf(longHours);

        String stringDuration = stringHours + "h:" + stringMinutes + "m:" + stringSeconds + "s";
        return stringDuration;
    }
}
