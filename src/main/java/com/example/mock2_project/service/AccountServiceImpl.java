package com.example.mock2_project.service;

import com.example.mock2_project.config.UserDetailsImpl;
import com.example.mock2_project.dto.request.AddingAccountRequest;
import com.example.mock2_project.dto.request.UpdatingAccountRequest;
import com.example.mock2_project.dto.response.*;
import com.example.mock2_project.entity.*;
import com.example.mock2_project.enums.AccessTokenType;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.AccessTokenRepository;
import com.example.mock2_project.repository.AccountInfoRepository;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.repository.RefreshTokenRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccountServiceImpl {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private FileDBService fileDBService;

    @Autowired
    private AccountInfoServiceImpl accountInfoServiceImpl;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private RefreshTokenServiceImpl refreshTokenServiceImpl;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    AccountInfoRepository accountInfoRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    AuthenticationServiceImpl authenticationServiceImpl;

    public List<AccountResponse> getAllAccount(Integer page, Integer size) {
        LOGGER.info("CALLING: GET ALL ACCOUNTS API");
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Account> accounts = accountRepository.findAll(pageable);

        if (accounts.getContent().isEmpty()) {
            LOGGER.warn("Accounts is empty!");
            throw new RuntimeException("No account found!");
        }

        LOGGER.info("Got account list!");
        return accounts.stream().map(
                acc -> {
                    ResponseFile responseFile = null;
                    AccountInfo accountInfo = acc.getAccountInfo();
                    List<Rating> ratings = acc.getRatings();
                    List<Invoice> invoices = acc.getInvoices();

                    if (acc.getFileDB() != null) {
                        FileDB fileDB = fileDBService.getFileById(acc.getFileDB().getId());

                        if (fileDB != null) {
                            String fileDownloadUri = "http://localhost:8080/account/file/" + fileDB.getId();

                            responseFile = new ResponseFile(
                                    fileDB.getName(),
                                    fileDownloadUri.toString(),
                                    fileDB.getType(),
                                    fileDB.getData().length);
                        }
                    }
                    AccountInfoResponse accountInfoResponse = new AccountInfoResponse(accountInfo.getId(), accountInfo.getPhone(), accountInfo.getDob(), accountInfo.getAddress());

                    RefreshTokenResponse refreshTokenResponse = null;
                    if (acc.getRefreshToken() != null) {
                        //RefreshToken refreshToken1 = refreshTokenRepository.findRefreshTokenByCreatedBy(acc.getEmail()).orElseThrow(() -> new EntityNotFoundException("rf token not found!"));
                        RefreshToken refreshToken = refreshTokenRepository.findByAccount_CreatedBy(acc.getEmail());
                        if (refreshToken != null) {
                            String rfToken = refreshToken.getRefreshToken();
                            String duration = refreshTokenServiceImpl.getDuration();
                            String remainingTime = refreshTokenServiceImpl.getRemainingTime(refreshToken);
                            refreshTokenResponse = new RefreshTokenResponse(rfToken, duration, remainingTime);
                        }
                    }

                    List<RatingResponse> ratingResponses = ratingService.getRatingsByProduct(page, size, acc.getId());

                    return new AccountResponse(acc.getId(), acc.getEmail(), acc.getFullName(), acc.getRole(), responseFile, accountInfoResponse, acc.getAccessTokens(), refreshTokenResponse, ratingResponses, null);
                }
        ).collect(Collectors.toList());
    }

    public AccountResponse getAccountById(long id, Integer page, Integer size) {
        LOGGER.info("CALLING: GET ACCOUNT BY ID API");
        return accountRepository.findById(id).map(
                acc -> {
                    ResponseFile responseFile = null;
                    AccountInfo accountInfo = acc.getAccountInfo();
                    //Neu comment ↙ thi se khong dung dto AccessTokenResponse later
                    List<AccessToken> accessToken = acc.getAccessTokens();
                    //RefreshToken refreshToken = acc.getRefreshToken();
                    List<Rating> ratings = acc.getRatings();
                    List<Invoice> invoices = acc.getInvoices();

                    if (acc.getFileDB() != null) {
                        FileDB fileDB = fileDBService.getFileById(acc.getFileDB().getId());

                        if (fileDB != null) {
                            String fileDownloadUri = "http://localhost:8080/account/file/" + fileDB.getId();

                            responseFile = new ResponseFile(
                                    fileDB.getName(),
                                    fileDownloadUri.toString(),
                                    fileDB.getType(),
                                    fileDB.getData().length);
                        }
                    }

                    AccountInfoResponse accountInfoResponse = new AccountInfoResponse(accountInfo.getId(), accountInfo.getPhone(), accountInfo.getDob(), accountInfo.getAddress());

                    RefreshTokenResponse refreshTokenResponse = null;
                    if (acc.getRefreshToken() != null) {
                        RefreshToken refreshToken = refreshTokenRepository.findByAccount_CreatedBy(acc.getEmail());
                        if (refreshToken != null) {
                            String rfToken = refreshToken.getRefreshToken();
                            String duration = refreshTokenServiceImpl.getDuration();
                            String remainingTime = refreshTokenServiceImpl.getRemainingTime(refreshToken);
                            refreshTokenResponse = new RefreshTokenResponse(rfToken, duration, remainingTime);
                        }
                    }

                    List<RatingResponse> ratingResponses = ratingService.getRatingsByProduct(page, size, acc.getId());

                    return new AccountResponse(acc.getId(), acc.getEmail(), acc.getFullName(), acc.getRole(), responseFile, accountInfoResponse, accessToken, refreshTokenResponse, ratingResponses, null);
                }
        ).orElseThrow(() -> {
            throw new EntityNotFoundException("Not found account with id: " + id);
        });
    }

    public Object getAccountByUsername(String email, Integer page, Integer size) {
        LOGGER.info("CALLING: GET ACCOUNT BY EMAIL API");
        return accountRepository.findByEmail(email).map(
                acc -> {
                    ResponseFile responseFile = null;
                    AccountInfo accountInfo = acc.getAccountInfo();
                    List<AccessToken> accessToken = acc.getAccessTokens();
                    //RefreshToken refreshToken = acc.getRefreshToken();
                    List<Rating> ratings = acc.getRatings();
                    List<Invoice> invoices = acc.getInvoices();

                    if (acc.getFileDB() != null) {
                        FileDB fileDB = fileDBService.getFileById(acc.getFileDB().getId());
                        if (fileDB != null) {
                            String fileDownloadUri = "http://localhost:8080/account/file/" + fileDB.getId();

                            responseFile = new ResponseFile(
                                    fileDB.getName(),
                                    fileDownloadUri.toString(),
                                    fileDB.getType(),
                                    fileDB.getData().length);
                        }
                    }

                    AccountInfoResponse accountInfoResponse = new AccountInfoResponse(accountInfo.getId(), accountInfo.getPhone(), accountInfo.getDob(), accountInfo.getAddress());

                    RefreshTokenResponse refreshTokenResponse = null;
                    if (acc.getRefreshToken() != null) {
                        RefreshToken refreshToken = refreshTokenRepository.findByAccount_CreatedBy(acc.getEmail());
                        if (refreshToken != null) {
                            String rfToken = refreshToken.getRefreshToken();
                            String duration = refreshTokenServiceImpl.getDuration();
                            String remainingTime = refreshTokenServiceImpl.getRemainingTime(refreshToken);
                            refreshTokenResponse = new RefreshTokenResponse(rfToken, duration, remainingTime);
                        }
                    }

                    List<RatingResponse> ratingResponses = ratingService.getRatingsByProduct(page, size, acc.getId());

                    return new AccountResponse(acc.getId(), acc.getEmail(), acc.getFullName(), acc.getRole(), responseFile, accountInfoResponse, accessToken, refreshTokenResponse, ratingResponses, null);
                }
        ).orElseThrow(() -> {
            throw new EntityNotFoundException("Not found account with email: " + email);
        });
    }

    public AccountResponse addAccount(MultipartFile file, String accountRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: ADD ACCOUNT API");
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            @Valid
            AddingAccountRequest addingAccountRequest = objectMapper.readValue(accountRequest, AddingAccountRequest.class);
            Account accountJwt = jwtProvider.getAccountFromToken(request);

            FileDB fileDB = fileDBService.saveFile(file, request);

            /*Account*/
            Account account = new Account();
            account.setCreatedAt(LocalDateTime.now());
            account.setCreatedBy(accountJwt.getEmail());
            account.setUpdatedAt(null);
            account.setUpdatedBy(null);
            account.setEmail(addingAccountRequest.getEmail());
            account.setPassword(passwordEncoder.encode(addingAccountRequest.getPassword()));
            account.setFullName(addingAccountRequest.getFullName());
            account.setRole(addingAccountRequest.getRole());
            account.setFileDB(fileDB);
            accountRepository.save(account);

            /*AccountInfo*/
            AccountInfo accountInfo = new AccountInfo();
            accountInfo.setAccount(account);
            accountInfo.setPhone(addingAccountRequest.getPhone());
            accountInfo.setDob(LocalDate.parse(addingAccountRequest.getDob()));
            accountInfo.setAddress(addingAccountRequest.getAddress());
            accountInfo.setCreatedAt(LocalDateTime.now());
            accountInfo.setCreatedBy(accountJwt.getEmail());
            accountInfo.setUpdatedAt(null);
            accountInfo.setUpdatedBy(null);
            accountInfoRepository.save(accountInfo);

            /*AccessToken*/
            AccessToken accessToken = new AccessToken();
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(addingAccountRequest.getEmail(), addingAccountRequest.getPassword());
            authenticationManager.authenticate(authenticationToken);
            UserDetailsImpl userDetailsImpl = userDetailsServiceImpl.loadUserByUsername(addingAccountRequest.getEmail());
            String token = jwtProvider.generateToken(userDetailsImpl);
            accessToken.setAccount(account);
            accessToken.setAccessToken(token);
            accessToken.setAccessTokenType(AccessTokenType.BEARER);
            accessToken.setExpired(false);
            accessToken.setRevoked(false);
            accessToken.setCreatedAt(LocalDateTime.now());
            accessToken.setCreatedBy(accountJwt.getEmail());
            accessToken.setUpdatedAt(null);
            accessToken.setUpdatedBy(null);
            authenticationServiceImpl.revokedAllAccessTokenForSpecificAccount(account);
            accessTokenRepository.save(accessToken);

            /*RefreshToken*/
            /*RefreshToken refreshToken = new RefreshToken();
            if (refreshTokenServiceImpl.checkRefreshTokenExistByCreator(addingAccountRequest.getEmail())) {
                refreshToken = refreshTokenServiceImpl.findRefreshTokenByCreator(addingAccountRequest.getEmail());
                if (!refreshToken.getExpiredDate().isBefore(LocalDateTime.now())) {
                    refreshTokenServiceImpl.deleteTokenByEmail(addingAccountRequest.getEmail());
                    refreshToken = refreshTokenServiceImpl.createRefreshTokenByUsername(userDetailsImpl.getUsername());
                }
            } else
                refreshToken = refreshTokenServiceImpl.createRefreshTokenByUsername(userDetailsImpl.getUsername());
            refreshToken.setCreatedAt(LocalDateTime.now());
            refreshToken.setCreatedBy(accountJwt.getEmail());
            refreshToken.setUpdatedAt(null);
            refreshToken.setUpdatedBy(null);
            refreshTokenRepository.save(refreshToken);*/

            ResponseFile responseFile = null;

            if (fileDB != null) {
                String fileDownloadUri = "http://localhost:8080/account/file/" + fileDB.getId().toString();

                responseFile = new ResponseFile(
                        fileDB.getName(),
                        fileDownloadUri.toString(),
                        fileDB.getType(),
                        fileDB.getData().length);
            }

            List<AccessToken> accessTokenList = new ArrayList<>();
            accessTokenList.add(accessToken);

            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getFullName(), account.getRole(), responseFile, accountInfo, accessTokenList, null, null, null);
            return accountResponse;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public AccountResponse updateAccount(MultipartFile file, String accountRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: UPDATE ACCOUNT API");
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            UpdatingAccountRequest updatingAccountRequest = objectMapper.readValue(accountRequest, UpdatingAccountRequest.class);

            Account account = accountRepository.findById(updatingAccountRequest.getId()).orElseThrow(() -> {
                throw new RuntimeException("No account found!");
            });

            FileDB fileDB;
            if (account.getFileDB() != null) {
                fileDB = fileDBService.updateFile(file, account.getFileDB().getId(), request);
            } else {
                fileDB = fileDBService.saveFile(file, request);
            }

            account.setEmail(updatingAccountRequest.getEmail());
            account.setPassword(passwordEncoder.encode(updatingAccountRequest.getPassword()));
            account.setFullName(updatingAccountRequest.getFullName());
            account.setRole(updatingAccountRequest.getRole());
            account.setFileDB(fileDB);

            ResponseFile responseFile = null;

            if (fileDB != null) {
                String fileDownloadUri = "http://localhost:8080/account/file/" + fileDB.getId();

                responseFile = new ResponseFile(
                        fileDB.getName(),
                        fileDownloadUri.toString(),
                        fileDB.getType(),
                        fileDB.getData().length);
            }

            accountRepository.save(account);

            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getFullName(), account.getRole(), responseFile, (AccountInfoResponse) null, account.getAccessTokens(), null, null, null);
            return accountResponse;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void deleteAccount(Long id) {
        LOGGER.info("CALLING: DELETE ACCOUNT BY ID API");
        Account account = accountRepository.findById(id).orElseThrow(() -> {
            throw new RuntimeException("No account found!");
        });

        try {
            List<AccessToken> accessTokens = accessTokenRepository.findAccessTokenByAccountId(account.getId());

            if (accessTokens != null) {
                for (AccessToken accessToken : accessTokens) {
                    accessTokenRepository.deleteById(accessToken.getId());
                }
                LOGGER.info("delete all access token {}", accessTokens);
                //log.info("delete all access token done! {}", accessTokens);
            }

            RefreshToken refreshToken = refreshTokenRepository.findByAccount_CreatedBy(account.getEmail());
            if (refreshToken != null) {
                refreshTokenRepository.deleteById(refreshToken.getId());
                //log.info("delete all refresh token done! {}", refreshToken);
                LOGGER.info("delete all refresh token {}", refreshToken);
            }

            AccountInfo accountInfo = accountInfoServiceImpl.getAccountInfoByAccountId(account.getId());
            if (accountInfo != null) {
                accountInfoServiceImpl.deleteAccountInfoById(accountInfo.getId());
                //log.info("delete all account info done! {}", accountInfo.getId());
            }

            FileDB fileDB = fileDBService.getFileDBByAccountId(account.getId());
            if (fileDB != null) {
                fileDBService.deleteFileDBById(fileDB.getId());
                //log.info("delete all file done! {}", fileDB);
            }

            //TODO: Ratings
            //TODO: Invoices

            accountRepository.deleteById(id);
            //log.info("delete account done! {}", account);
            LOGGER.info("account deleted {}", account);
        } catch (Exception e) {
            LOGGER.info("Delete product fail! {}", account);
            throw new RuntimeException("Can't delete");
        }
    }
}
