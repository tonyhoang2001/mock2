package com.example.mock2_project.service;

import com.example.mock2_project.dto.request.CategoryRequest;
import com.example.mock2_project.dto.response.CategoryResponse;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.Category;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.CategoryRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private JwtProvider jwtProvider;

    public List<CategoryResponse> getAllCategories(Integer page, Integer size) {
        LOGGER.info("CALLING: GET ALL CATEGORIES API");
        Pageable pageable = PageRequest.of(page - 1, size);
        List<Category> categories = categoryRepository.findAll(pageable).getContent();
        if (categories.isEmpty()) {
            LOGGER.warn("Category list is emty!");
            return null;
        }
        LOGGER.info("Got category list!");
        return categories.stream().map(
                c -> new CategoryResponse(c.getId(), c.getCategoryName())
        ).collect(Collectors.toList());
    }

    public CategoryResponse getCategoryById(long id) {
        LOGGER.info("CALLING: GET CATEGORY BY ID API with ID = " + id);
        Optional<Category> category = categoryRepository.findById(id);
        if (!category.isPresent()) {
            LOGGER.warn("Not found category with ID = " + id);
            return null;
        }
        return categoryRepository.findById(id)
                .map(c -> new CategoryResponse(c.getId(), c.getCategoryName()))
                .get();
    }

    public CategoryResponse addCategory(CategoryRequest categoryRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: ADD CATEGORY API");
        Account account = jwtProvider.getAccountFromToken(request);
        try {
            Category category = categoryRepository.save(new Category(LocalDateTime.now(), null, account.getEmail(), null, categoryRequest.getCategoryName()));
            LOGGER.info("Add category successful!");
            return new CategoryResponse(category.getId(), category.getCategoryName());
        } catch (Exception e) {
            LOGGER.error("Add category fail!");
            return null;
        }
    }

    public CategoryResponse updateCategory(CategoryRequest categoryRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: UPDATE CATEGORY API");
        Optional<Category> c = categoryRepository.findById(categoryRequest.getId());
        if (!c.isPresent()) {
            LOGGER.error("No category found!");
            throw new RuntimeException("No category found!");
        }

        Account account = jwtProvider.getAccountFromToken(request);
        Category category = c.get();
        category.setCategoryName(category.getCategoryName());
        category.setUpdatedAt(LocalDateTime.now());
        category.setUpdatedBy(account.getEmail());

        try {
            categoryRepository.save(category);
            LOGGER.info("Update category successful!");
            return new CategoryResponse(category.getId(), category.getCategoryName());
        } catch (Exception e) {
            LOGGER.error("Update category fail!");
            return null;
        }
    }

    public boolean deleteCategory(long id) {
        LOGGER.info("CALLING: DELETE CATEGORY API");
        Category category = categoryRepository.findById(id).orElseThrow(() -> {
            LOGGER.error("Not found category with id: " + id);
            throw new RuntimeException("Not found category with id: " + id);
        });
        try {
            categoryRepository.delete(category);
            LOGGER.info("Delete category successful!");
            return true;
        } catch (Exception e) {
            LOGGER.error("Delete category fail!");
            return false;
        }
    }

}
