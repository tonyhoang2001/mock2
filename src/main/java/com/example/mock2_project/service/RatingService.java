package com.example.mock2_project.service;

import com.example.mock2_project.dto.request.AddingRatingRequest;
import com.example.mock2_project.dto.request.UpdatingRatingRequest;
import com.example.mock2_project.dto.response.AccountResponse;
import com.example.mock2_project.dto.response.RatingResponse;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.Product;
import com.example.mock2_project.entity.Rating;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.repository.ProductRepository;
import com.example.mock2_project.repository.RatingRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RatingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RatingService.class);
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private JwtProvider jwtProvider;

//    public List<RatingResponse> getAllRatings(Integer page, Integer size) {
//        Pageable pageable = PageRequest.of(page - 1, size);
//        Page<Rating> ratings = ratingRepository.findAll(pageable);
//
//        if (ratings.getContent().isEmpty()) {
//            throw new RuntimeException("No Rating found!");
//        }
//
//        return ratings.stream().map(rt -> {
//            Account account = rt.getAccount();
//            Product product = rt.getProduct();
//            Category category = product.getCategory();
//
//            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getRole(), account.getFullName());
//            AddingRatingProductResponse productResponse = new AddingRatingProductResponse(product.getId(), product.getName(), product.getDescription(), product.getPrice(),
//                    product.getQuantity(), new CategoryResponse(category.getId(), category.getCategoryName()));
//
//            return new RatingResponse(rt.getId(), rt.getComment(), accountResponse, productResponse);
//        }).collect(Collectors.toList());
//    }

    public List<RatingResponse> getRatingsByProduct(Integer page, Integer size, Long productId) {
        LOGGER.info("CALLING: GET RATINGS BY PRODUCT API");
        List<Rating> ratings;
        if (page == 0 && size == 0) {
            ratings = ratingRepository.findRatingsByProductId(productId);
        } else {
            Pageable pageable = PageRequest.of(page - 1, size);
            ratings = ratingRepository.findRatingsByProductId(productId, pageable).getContent();
        }

        if (ratings.isEmpty()) {
            LOGGER.warn("Rating list is empty");
            return null;
        }

        LOGGER.info("Got rating list!");
        return ratings.stream().map(rt -> {
            Account account = rt.getAccount();

            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getRole(), account.getFullName());

            return new RatingResponse(rt.getId(), rt.getComment(), accountResponse);
        }).collect(Collectors.toList());
    }

//    public RatingResponse getRatingById(Long id) {
//        Optional<Rating> rating = ratingRepository.findById(id);
//
//        if (!rating.isPresent()) {
//            throw new RuntimeException("No Rating found!");
//        }
//
//        return rating.map(rt -> {
//            Account account = rt.getAccount();
//            Product product = rt.getProduct();
//            Category category = product.getCategory();
//
//            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getRole(), account.getFullName());
//            AddingRatingProductResponse productResponse = new AddingRatingProductResponse(product.getId(), product.getName(), product.getDescription(), product.getPrice(),
//                    product.getQuantity(), new CategoryResponse(category.getId(), category.getCategoryName()));
//
//            return new RatingResponse(rt.getId(), rt.getComment(), accountResponse, productResponse);
//        }).get();
//    }

    public RatingResponse addRating(AddingRatingRequest ratingRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: ADD RATINGS API");
        Product product = productRepository.findById(ratingRequest.getProductId()).orElseThrow(
                () -> {
                    LOGGER.error("Not existed product!");
                    throw new RuntimeException("Not existed product!");
                });
        try {
            Account account = jwtProvider.getAccountFromToken(request);
            Rating rating = ratingRepository.save(new Rating(LocalDateTime.now(), null, account.getEmail(), null, ratingRequest.getComment(), account, product));

            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getRole(), account.getFullName());
            LOGGER.info("Add product successful!");
            return new RatingResponse(rating.getId(), ratingRequest.getComment(), accountResponse);
        } catch (Exception e) {
            LOGGER.error("Add product fail!");
            return null;
        }
    }

    public RatingResponse updateRating(UpdatingRatingRequest ratingRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: UPDATE RATINGS API");
        Rating rating = ratingRepository.findById(ratingRequest.getId()).orElseThrow(
                () -> {
                    LOGGER.error("Not found rating");
                    throw new RuntimeException("Not found rating!");
                }
        );
        try {
            Account curAccount = jwtProvider.getAccountFromToken(request);
            rating.setComment(ratingRequest.getComment());
            rating.setUpdatedAt(LocalDateTime.now());
            rating.setUpdatedBy(curAccount.getEmail());
            ratingRepository.save(rating);

            Account account = rating.getAccount();
            AccountResponse accountResponse = new AccountResponse(account.getId(), account.getEmail(), account.getRole(), account.getFullName());
            LOGGER.info("Update rating successul!");
            return new RatingResponse(rating.getId(), rating.getComment(), accountResponse);
        } catch (Exception e) {
            LOGGER.error("Update rating fail!");
            return null;
        }
    }

    public boolean deleteRating(Long id) {
        LOGGER.info("CALLING: DELETE RATINGS API");
        Rating rating = ratingRepository.findById(id).orElseThrow(
                () -> {
                    LOGGER.error("Not found rating");
                    throw new RuntimeException("Not found rating!");
                }
        );
        try {
            ratingRepository.delete(rating);
            LOGGER.info("Delete rating successful!");
            return true;
        } catch (Exception e) {
            LOGGER.error("Delete rating fail!");
            return false;
        }
    }


}
