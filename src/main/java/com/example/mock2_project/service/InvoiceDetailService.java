package com.example.mock2_project.service;

import com.example.mock2_project.dto.request.InvoiceDetailRequest;
import com.example.mock2_project.dto.response.InvoiceDetailResponse;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.Invoice;
import com.example.mock2_project.entity.InvoiceDetail;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.InvoiceDetailRepository;
import com.example.mock2_project.repository.InvoiceRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InvoiceDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceDetailService.class);
    @Autowired
    private InvoiceDetailRepository invoiceDetailRepository;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private JwtProvider jwtProvider;

    public List<InvoiceDetailResponse> getByInvoiceId(Integer page, Integer size, Long invoiceId) {
        LOGGER.info("CALLING: GET INVOICE DETAIL BY INVOICE API");
        Optional<Invoice> invoice = invoiceRepository.findById(invoiceId);
        if (!invoice.isPresent()) {
            LOGGER.warn("No invoice detail found!");
            return null;
        }

        LOGGER.info("Found invoice detail!");
        Pageable pageable = PageRequest.of(page - 1, size);
        List<InvoiceDetail> invoiceDetails = invoiceDetailRepository.findInvoiceDetailsByInvoice_Id(invoiceId, pageable).getContent();

        return invoiceDetails.stream().map(
                dt -> new InvoiceDetailResponse(dt.getId(), dt.getQuantity(), dt.getTotalPrice(), dt.getProduct().getId())
        ).collect(Collectors.toList());
    }

    public InvoiceDetailResponse getById(Long id) {
        LOGGER.info("CALLING: GET INVOICE DETAIL BY ID API");
        Optional<InvoiceDetail> invoiceDetail = invoiceDetailRepository.findById(id);
        if (!invoiceDetail.isPresent()) {
            LOGGER.warn("No invoice detail found!");
            return null;
        }

        LOGGER.info("Found invoice detail!");
        return invoiceDetail.map(
                dt -> new InvoiceDetailResponse(dt.getId(), dt.getQuantity(), dt.getTotalPrice(), dt.getProduct().getId())
        ).get();
    }

    public InvoiceDetailResponse update(InvoiceDetailRequest detailRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: UPDATE INVOICE DETAIL API");
        InvoiceDetail invoiceDetail = invoiceDetailRepository.findById(detailRequest.getId()).orElseThrow(() -> {
            LOGGER.error("No invoice detail found!");
            throw new RuntimeException("No invoice detail found!");
        });

        try {
            Account account = jwtProvider.getAccountFromToken(request);
            invoiceDetail.setUpdatedBy(account.getEmail());
            invoiceDetail.setUpdatedAt(LocalDateTime.now());
            invoiceDetail.setTotalPrice(detailRequest.getTotalPrice());
            invoiceDetail.setQuantity(detailRequest.getQuantity());
            invoiceDetailRepository.save(invoiceDetail);
            LOGGER.info("Update invoice detail successful!");
            return new InvoiceDetailResponse(invoiceDetail.getId(), invoiceDetail.getQuantity(), invoiceDetail.getTotalPrice(), invoiceDetail.getProduct().getId());
        } catch (Exception e) {
            LOGGER.error("Update invoice detail fail!");
            return null;
        }

    }

    public boolean delete(Long id) {
        LOGGER.info("CALLING: DELETE INVOICE DETAIL API");
        InvoiceDetail invoiceDetail = invoiceDetailRepository.findById(id).orElseThrow(() -> {
            LOGGER.error("No invoice detail found!");
            throw new RuntimeException("No invoice detail found!");
        });
        try {
            invoiceDetailRepository.delete(invoiceDetail);
            LOGGER.info("Update invoice detail successful!");
            return true;
        } catch (Exception e) {
            LOGGER.error("Update invoice detail fail!");
            return false;
        }
    }

}
