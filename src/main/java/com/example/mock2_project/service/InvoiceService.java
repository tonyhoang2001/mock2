package com.example.mock2_project.service;

import com.example.mock2_project.dto.request.InvoiceDetailRequest;
import com.example.mock2_project.dto.request.InvoiceRequest;
import com.example.mock2_project.dto.response.InvoiceDetailResponse;
import com.example.mock2_project.dto.response.InvoiceResponse;
import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.Invoice;
import com.example.mock2_project.entity.InvoiceDetail;
import com.example.mock2_project.entity.Product;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.repository.InvoiceDetailRepository;
import com.example.mock2_project.repository.InvoiceRepository;
import com.example.mock2_project.repository.ProductRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class InvoiceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceService.class);
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private InvoiceDetailRepository invoiceDetailRepository;

    public List<InvoiceResponse> getAllByAccount(Integer page, Integer size, Long accountId) {
        LOGGER.info("CALLING: GET ALL INVOICE BY ACCOUNT API");
        Pageable pageable = PageRequest.of(page - 1, size);
        List<Invoice> invoices = invoiceRepository.findInvoiceByAccount_Id(accountId, pageable).getContent();
        if (invoices.isEmpty()) {
            LOGGER.warn("Invoice list is empty!");
            return null;
        }
        LOGGER.info("Got invoice list!");
        return invoices.stream().map(
                i -> {
                    List<InvoiceDetail> invoiceDetails = i.getInvoiceDetails();
                    List<InvoiceDetailResponse> invoiceDetailResponses = invoiceDetails.stream()
                            .map(dt -> new InvoiceDetailResponse(dt.getId(), dt.getQuantity(), dt.getTotalPrice(), dt.getProduct().getId()))
                            .collect(Collectors.toList());
                    return new InvoiceResponse(i.getId(), i.getTotal(), i.getExpectedDate(), i.getShippedDate(), invoiceDetailResponses);
                }
        ).collect(Collectors.toList());
    }

    public InvoiceResponse getById(Long id) {
        LOGGER.info("CALLING: GET INVOICE BY ID API");
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if (!invoice.isPresent()) {
            LOGGER.warn("Invoice not found");
            return null;
        }

        LOGGER.info("Invoice found!");
        return invoice.map(
                i -> {
                    List<InvoiceDetail> invoiceDetails = i.getInvoiceDetails();
                    List<InvoiceDetailResponse> invoiceDetailResponses = invoiceDetails.stream()
                            .map(dt -> new InvoiceDetailResponse(dt.getId(), dt.getQuantity(), dt.getTotalPrice(), dt.getProduct().getId()))
                            .collect(Collectors.toList());
                    return new InvoiceResponse(i.getId(), i.getTotal(), i.getExpectedDate(), i.getShippedDate(), invoiceDetailResponses);
                }
        ).get();
    }

    public InvoiceResponse addInvoice(InvoiceRequest invoiceRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: ADD INVOICE API");
        List<InvoiceDetailRequest> invoiceDetails = invoiceRequest.getInvoiceDetails();
        if (invoiceDetails == null || invoiceDetails.isEmpty()) {
            LOGGER.error("Must add invoice details!");
            throw new RuntimeException("Must add invoice details!");
        }

        try {
            Account account = jwtProvider.getAccountFromToken(request);

            Invoice invoice = new Invoice(LocalDateTime.now(), null, account.getEmail(), null, invoiceRequest.getTotal(), invoiceRequest.getExpectedDate(), invoiceRequest.getShippedDate(), account);

            List<InvoiceDetail> idts = new ArrayList<>();
            for (InvoiceDetailRequest idt : invoiceRequest.getInvoiceDetails()) {
                Product product = productRepository.findById(idt.getProductId()).orElseThrow(() -> {
                    LOGGER.error("No product found!");
                    throw new RuntimeException("No product found!");
                });
                idts.add(new InvoiceDetail(LocalDateTime.now(), null, account.getEmail(), null, idt.getQuantity(), idt.getTotalPrice(), null, product));
            }

            List<InvoiceDetail> savedInvoiceDTs = invoiceDetailRepository.saveAll(idts);
            invoice.setInvoiceDetails(savedInvoiceDTs);
            Invoice savedInvoice = invoiceRepository.save(invoice);

            for (InvoiceDetail invoiceDetail : savedInvoiceDTs) {
                invoiceDetail.setInvoice(savedInvoice);
            }

            List<InvoiceDetailResponse> invoiceDetailResponses = savedInvoiceDTs.stream().map(
                    dt -> new InvoiceDetailResponse(dt.getId(), dt.getQuantity(), dt.getTotalPrice(), dt.getProduct().getId())
            ).collect(Collectors.toList());
            LOGGER.info("Add invoice successful!");
            return new InvoiceResponse(savedInvoice.getId(), savedInvoice.getTotal(), savedInvoice.getExpectedDate(), savedInvoice.getShippedDate(), invoiceDetailResponses);
        } catch (Exception e) {
            LOGGER.error("Add invoice fail!");
            return null;
        }
    }

    public InvoiceResponse updateInvoice(InvoiceRequest invoiceRequest, HttpServletRequest request) {
        LOGGER.info("CALLING: UPDATE INVOICE API");
        Invoice invoice = invoiceRepository.findById(invoiceRequest.getId()).orElseThrow(() -> {
            LOGGER.error("No invoice found!");
            throw new RuntimeException("No invoice found!");
        });
        if (invoice.getShippedDate() != null) {
            LOGGER.error("This invoice was shipped, Cannot modify!!");
            throw new RuntimeException("This invoice was shipped, Cannot modify!!");
        }

        try {
            Account account = jwtProvider.getAccountFromToken(request);

            invoice.setUpdatedAt(LocalDateTime.now());
            invoice.setUpdatedBy(account.getEmail());

            invoice.setExpectedDate(invoiceRequest.getExpectedDate());
            invoice.setShippedDate(invoiceRequest.getShippedDate());
            invoice.setTotal(invoiceRequest.getTotal());

            if (invoiceRequest.getInvoiceDetails() != null) {
                for (InvoiceDetailRequest detailRequest : invoiceRequest.getInvoiceDetails()) {
                    Product product = productRepository.findById(detailRequest.getProductId()).orElseThrow(() -> {
                        LOGGER.error("No product found!");
                        throw new RuntimeException("No product found!");
                    });
                    InvoiceDetail invoiceDetail = new InvoiceDetail(LocalDateTime.now(), null, account.getEmail(), null, detailRequest.getQuantity(), detailRequest.getTotalPrice(), invoice, product);
                    invoiceDetailRepository.save(invoiceDetail);
                }
            }

            List<InvoiceDetail> invoiceDetails = invoiceDetailRepository.findInvoiceDetailsByInvoice_Id(invoiceRequest.getId());
            List<InvoiceDetailResponse> detailResponses = invoiceDetails.stream().map(
                    dt -> {
                        dt.setUpdatedAt(LocalDateTime.now());
                        dt.setUpdatedBy(account.getEmail());

                        return new InvoiceDetailResponse(dt.getId(), dt.getQuantity(), dt.getTotalPrice(), dt.getProduct().getId());
                    }).collect(Collectors.toList());
            LOGGER.info("Update product successful!");
            return new InvoiceResponse(invoice.getId(), invoice.getTotal(), invoice.getExpectedDate(), invoice.getShippedDate(), detailResponses);
        } catch (Exception e) {
            LOGGER.error("Update product fail!");
            return null;
        }
    }

    public boolean deleteInvoice(Long id) {
        LOGGER.info("CALLING: DELETE INVOICE API");
        Invoice invoice = invoiceRepository.findById(id).orElseThrow(() -> {
            LOGGER.error("No invoice found");
            throw new RuntimeException("No invoice found!");
        });

        try {
            invoiceRepository.delete(invoice);
            LOGGER.info("Delete invoice successful!");
            return true;
        } catch (Exception e) {
            LOGGER.error("Delete invoice fail!");
            return false;
        }
    }


}
