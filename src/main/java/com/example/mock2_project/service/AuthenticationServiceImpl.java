package com.example.mock2_project.service;

import com.example.mock2_project.config.UserDetailsImpl;
import com.example.mock2_project.dto.request.LoginRequest;
import com.example.mock2_project.dto.request.RegisterRequest;
import com.example.mock2_project.dto.request.ReLoginRequest;
import com.example.mock2_project.dto.response.LoginResponse;
import com.example.mock2_project.dto.response.ReLoginResponse;
import com.example.mock2_project.dto.response.RegisterResponse;
import com.example.mock2_project.entity.*;
import com.example.mock2_project.enums.AccessTokenType;
import com.example.mock2_project.exception.TokenNotFoundException;
import com.example.mock2_project.exception.TokenTimeOutException;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.AccessTokenRepository;
import com.example.mock2_project.repository.AccountInfoRepository;
import com.example.mock2_project.repository.AccountRepository;
import com.example.mock2_project.validation.RegisterValidation;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class AuthenticationServiceImpl {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountInfoRepository accountInfoRepository;

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    private RefreshTokenServiceImpl refreshTokenServiceImpl;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    private JwtProvider jwtProvider;


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private FileDBService fileDBService;

    @Autowired
    private RegisterValidation registerValidation;


    public RegisterResponse serviceRegister(MultipartFile file, RegisterRequest registerRequest, HttpServletRequest request) throws Exception {

        registerValidation.validateAccountRegistration(registerRequest);

        Account account = new Account();
        AccountInfo accountInfo = new AccountInfo();
        AccessToken accessToken = new AccessToken();
        RegisterResponse registerResponse = new RegisterResponse();
        //FileDB fileDB = fileDBService.saveFile(file, request);

        /*Account*/
        account.setEmail(registerRequest.getEmail());
        account.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        account.setFullName(registerRequest.getName());
        account.setFileDB(null);
        account.setRole(registerRequest.getRole());
        account.setCreatedAt(LocalDateTime.now());
        account.setCreatedBy(registerRequest.getEmail());
        account.setUpdatedAt(null);
        account.setUpdatedBy(null);
        accountRepository.save(account);

        /*AccountInfo*/
        accountInfo.setAccount(account);
        accountInfo.setPhone(registerRequest.getPhone());
        accountInfo.setDob(LocalDate.parse(registerRequest.getDob()));
        accountInfo.setAddress(registerRequest.getAddress());
        accountInfo.setCreatedAt(LocalDateTime.now());
        accountInfo.setCreatedBy(registerRequest.getEmail());
        accountInfo.setUpdatedAt(null);
        accountInfo.setUpdatedBy(null);
        accountInfoRepository.save(accountInfo);

        /*AccessToken*/
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(registerRequest.getEmail(), registerRequest.getPassword());
        authenticationManager.authenticate(authenticationToken);
        UserDetailsImpl userDetailsImpl = userDetailsServiceImpl.loadUserByUsername(registerRequest.getEmail());
        String token = jwtProvider.generateToken(userDetailsImpl);
        accessToken.setAccount(account);
        accessToken.setAccessToken(token);
        accessToken.setAccessTokenType(registerRequest.getAccessTokenType());
        accessToken.setExpired(false);
        accessToken.setRevoked(false);
        accessToken.setCreatedAt(LocalDateTime.now());
        accessToken.setCreatedBy(registerRequest.getEmail());
        accessToken.setUpdatedAt(null);
        accessToken.setUpdatedBy(null);
        revokedAllAccessTokenForSpecificAccount(account);
        accessTokenRepository.save(accessToken);


        /*RegisterResponse*/
        registerResponse.setEmail(account.getEmail());
        registerResponse.setPassword(account.getPassword());
        registerResponse.setName(account.getFullName());
        //registerResponse.setFileDB(null);
        Collection<? extends GrantedAuthority> roles = userDetailsImpl.getAuthorities();
        registerResponse.setRole(roles);
        registerResponse.setPhone(accountInfo.getPhone());
        registerResponse.setDob(accountInfo.getDob());
        registerResponse.setAddress(accountInfo.getAddress());
        //registerResponse.setAccessToken(accessToken.getAccessToken());
        //registerResponse.setAccessTokenType(accessToken.getAccessTokenType());
        //registerResponse.setExpired(accessToken.isExpired());
        //registerResponse.setRevoked(accessToken.isRevoked());

        return registerResponse;
    }

    public LoginResponse serviceLogin(LoginRequest loginRequest) throws TokenNotFoundException, TokenTimeOutException {
        LoginResponse loginResponse = new LoginResponse();

        /*AccessToken*/
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword());
        authenticationManager.authenticate(authenticationToken);
        UserDetailsImpl userDetailsImpl = userDetailsServiceImpl.loadUserByUsername(loginRequest.getEmail());
        String token = jwtProvider.generateToken(userDetailsImpl);
        AccessToken accessToken = new AccessToken();
        Account account = accountRepository.findByEmail(loginRequest.getEmail()).orElseThrow();
        accessToken.setAccount(account);
        accessToken.setAccessToken(token);
        accessToken.setAccessTokenType(AccessTokenType.BEARER);
        accessToken.setExpired(false);
        accessToken.setRevoked(false);
        accessToken.setCreatedAt(LocalDateTime.now());
        accessToken.setCreatedBy(loginRequest.getEmail());
        accessToken.setUpdatedAt(null);
        accessToken.setUpdatedBy(null);
        revokedAllAccessTokenForSpecificAccount(account);
        accessTokenRepository.save(accessToken);

        /*RefreshToken*/
        RefreshToken refreshToken;
        if (refreshTokenServiceImpl.checkRefreshTokenExistByCreator(loginRequest.getEmail())) {
            refreshToken = refreshTokenServiceImpl.findRefreshTokenByCreator(loginRequest.getEmail());
            if (refreshToken.getExpiredDate().isBefore(LocalDateTime.now())){
                System.out.println("token van con han su dung");
            } else {
                refreshTokenServiceImpl.deleteTokenByEmail(loginRequest.getEmail());
                refreshToken = refreshTokenServiceImpl.createRefreshTokenByUsername(userDetailsImpl.getUsername());
            }
        } else
            refreshToken = refreshTokenServiceImpl.createRefreshTokenByUsername(userDetailsImpl.getUsername());

        refreshToken.setCreatedAt(LocalDateTime.now());
        refreshToken.setCreatedBy(loginRequest.getEmail());
        refreshToken.setUpdatedAt(null);
        refreshToken.setUpdatedBy(null);

        /*email, roles*/
        String email = userDetailsImpl.getUsername();
        Collection<? extends GrantedAuthority> roles = userDetailsImpl.getAuthorities();

        /*LoginResponse*/
        loginResponse.setNewAccessToken(token);
        loginResponse.setNewRefreshToken(refreshToken);
        loginResponse.setEmail(email);
        loginResponse.setRole(roles);
        loginResponse.setRefreshTokenDuration(refreshTokenServiceImpl.getDuration());
        loginResponse.setRefreshTokenRemainingTime(refreshTokenServiceImpl.getRemainingTime(refreshToken));
        return loginResponse;
    }

    public ReLoginResponse serviceReLogin(ReLoginRequest reLoginRequest) throws TokenTimeOutException, TokenNotFoundException {
        String requestRefreshToken = reLoginRequest.getRefreshToken();
        RefreshToken refreshToken = refreshTokenServiceImpl.findByToken(requestRefreshToken);
        refreshTokenServiceImpl.isRefreshTokenExpired(refreshToken);


        /*AccessToken*/
        Account account = refreshToken.getAccount();
        UserDetailsImpl userDetails = new UserDetailsImpl(account);
        String token = jwtProvider.generateToken(userDetails);
        AccessToken accessToken = new AccessToken();
        accessToken.setAccessToken(token);
        accessToken.setAccount(account);
        accessToken.setAccessTokenType(AccessTokenType.BEARER);
        accessToken.setExpired(false);
        accessToken.setRevoked(false);
        accessToken.setCreatedAt(LocalDateTime.now());
        accessToken.setCreatedBy(account.getEmail());
        accessToken.setUpdatedAt(null);
        accessToken.setUpdatedBy(null);
        revokedAllAccessTokenForSpecificAccount(account);
        accessTokenRepository.save(accessToken);

        /*ReLoginResponse*/
        ReLoginResponse reLoginResponse = new ReLoginResponse();
        reLoginResponse.setCurrentRefreshToken(requestRefreshToken);
        reLoginResponse.setNewAccessToken(token);
        reLoginResponse.setDuration(refreshTokenServiceImpl.getDuration());
        reLoginResponse.setRemainingTime(refreshTokenServiceImpl.getRemainingTime(refreshToken));

        return reLoginResponse;
    }

    public void revokedAllAccessTokenForSpecificAccount(Account account) {
        List<AccessToken> validAccessTokens = accessTokenRepository.findAllAvailableAccessTokenByAccountId(account.getId());
        if (validAccessTokens.isEmpty())
            return;
        validAccessTokens.forEach(token -> {token.setRevoked(true); token.setExpired(true);});
        accessTokenRepository.saveAll(validAccessTokens);
    }

}
