package com.example.mock2_project.service;

import com.example.mock2_project.entity.AccessToken;
import com.example.mock2_project.jwt.JwtProvider;
import com.example.mock2_project.repository.AccessTokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LogoutServiceImpl implements LogoutHandler {

    @Autowired
    private AccessTokenRepository accessTokenRepository;

    @Autowired
    JwtProvider jwtProvider;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        final String authHeader = request.getHeader("Authorization");
        final String jwt;

        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            return;
        }
        jwt = authHeader.substring(7);
        AccessToken storedToken = accessTokenRepository.findByAccessToken(jwt).orElse(null);
        if (storedToken != null) {
            storedToken.setExpired(true);
            storedToken.setRevoked(true);
            accessTokenRepository.save(storedToken);
        }

        String userEmail = jwtProvider.extractUsername(jwt);
        log.info("Logged-out from {}", userEmail);
    }
}
