package com.example.mock2_project.service;

import com.example.mock2_project.entity.AccountInfo;
import com.example.mock2_project.repository.AccountInfoRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AccountInfoServiceImpl {
    @Autowired
    AccountInfoRepository accountInfoRepository;

    public List<AccountInfo> getAllAccountInfos(Integer page, Integer size) throws Exception {
        Pageable pageable = PageRequest.of(page-1, size);
        List<AccountInfo> accountInfos = accountInfoRepository.findAll(pageable).getContent();
        if (accountInfos.isEmpty()){
            throw new Exception("No account info found!");
        }
        return accountInfos;
    }

    public AccountInfo getAccountInfoById(long id) {
        return accountInfoRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Not found AccountInfoController with ID: " + id));
    }

    public AccountInfo getAccountInfoByAccountEmail(String accountEmail) {
        return accountInfoRepository.findAccountInfoByAccountEmail(accountEmail).orElseThrow(() -> new EntityNotFoundException("Not found any information with email " + accountEmail));
    }

    public AccountInfo getAccountInfoByAccountId(Long id) {
        return accountInfoRepository.findAccountInfoByAccountId(id);
    }

    public AccountInfo addAccountInfo(AccountInfo accountInfo) {
        Optional<AccountInfo> ai = accountInfoRepository.findAccountInfoByAccountEmail(accountInfo.getAccount().getEmail());
        if (ai.isPresent()){
            throw new RuntimeException("Account is already exist: " + accountInfo.getAccount().getEmail());
        }
        return accountInfoRepository.save(accountInfo);
    }

    public AccountInfo updateAccountInfoByAccountEmail(AccountInfo accountInfo) {
        Optional<AccountInfo> ai = accountInfoRepository.findAccountInfoByAccountEmail(accountInfo.getAccount().getEmail());
        if (ai.isPresent()){
            throw new RuntimeException("There's some information already existed with email: " + accountInfo.getAccount().getEmail());
        }
        return accountInfoRepository.save(accountInfo);
    }

    public void deleteAccountInfoById(long id) {
        Optional<AccountInfo> accountInfo = accountInfoRepository.findById(id);
        if (!accountInfo.isPresent()){
            throw new EntityNotFoundException("Not found any account information with id: " + id);
        }
        accountInfoRepository.deleteById(id);
    }

    public void deleteAccountInfoByAccountEmail(String accountEmail) {
        Optional<AccountInfo> accountInfo = accountInfoRepository.findAccountInfoByAccountEmail(accountEmail);
        if (!accountInfo.isPresent()){
            throw new EntityNotFoundException("Not found any information with email " + accountEmail);
        }
        accountInfoRepository.deleteAccountInfoByAccountEmail(accountEmail);
    }
}
