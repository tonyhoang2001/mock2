package com.example.mock2_project.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum Role {
    //Code role cua Tuan - tham khao CMC Global
    USER("USER"),
    ADMIN("ADMIN");

    private static final Map<String, Role> mainSkillMap = new HashMap<>();

    static {
        for (Role mainSkill : Role.values()) {
            mainSkillMap.put(mainSkill.value, mainSkill);
        }
    }

    private String value;


    //Them @JsonCreator de xu ly Exception
    //Exception: JSON parse error: Cannot deserialize value of type...not one of the values accepted for Enum class...
    @JsonCreator
    public static Role of(String s) {
        return mainSkillMap.get(s);
    }
    //ADMIN, USER
}
