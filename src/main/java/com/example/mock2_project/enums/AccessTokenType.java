package com.example.mock2_project.enums;

public enum AccessTokenType {
    BEARER, OTHER
}
