package com.example.mock2_project.repository;

import com.example.mock2_project.entity.Invoice;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    Page<Invoice> findInvoiceByAccount_Id(Long accountId, Pageable pageable);
    List<Invoice> findInvoiceByAccount_Id(Long accountId);
}
