package com.example.mock2_project.repository;

import com.example.mock2_project.entity.Rating;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {

    Page<Rating> findRatingsByProductId(Long productId, Pageable pageable);

    List<Rating> findRatingsByProductId(Long productId);
}
