package com.example.mock2_project.repository;

import com.example.mock2_project.entity.InvoiceDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetail, Long> {
    List<InvoiceDetail> findInvoiceDetailsByInvoice_Id(Long invoiceId);
    Page<InvoiceDetail> findInvoiceDetailsByInvoice_Id(Long invoiceId, Pageable pageable);
}
