package com.example.mock2_project.repository;

import com.example.mock2_project.entity.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccessTokenRepository extends JpaRepository<AccessToken, Long> {
    @Query("""
            select t from access_token t inner join account a on t.account.id = a.id
            where a.id = :accountId and (t.isExpired = false or t.isRevoked = false )
            """)
    List<AccessToken> findAllAvailableAccessTokenByAccountId(Long accountId);

    List<AccessToken> findAccessTokenByAccountId(Long accountId);

    Optional<AccessToken> findByAccessToken(String token);
}
