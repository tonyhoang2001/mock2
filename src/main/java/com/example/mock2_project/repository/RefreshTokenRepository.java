package com.example.mock2_project.repository;

import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.RefreshToken;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Long> {
    Optional<RefreshToken> findByRefreshToken(String refreshToken);

    Optional<RefreshToken> findRefreshTokenByCreatedBy(String email);

    RefreshToken findByAccount_CreatedBy(String email);

    @Modifying
    int deleteRefreshTokenByAccount(Account account);

    @Modifying
    void deleteRefreshTokenByCreatedBy(String email);
}
