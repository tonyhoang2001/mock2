package com.example.mock2_project.repository;

import com.example.mock2_project.entity.Account;
import com.example.mock2_project.entity.AccountInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountInfoRepository extends JpaRepository<AccountInfo, Long> {

    @Query("""
            select ai from account_info ai inner join account a on ai.account.id = a.id
            where a.email = ?1
            """)
    Optional<AccountInfo> findAccountInfoByAccountEmail(String accountEmail);

    void deleteAccountInfoByAccountEmail(String accountEmail);

    AccountInfo findAccountInfoByAccountId(Long id);
}
