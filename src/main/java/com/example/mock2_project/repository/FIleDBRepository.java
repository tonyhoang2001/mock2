package com.example.mock2_project.repository;

import com.example.mock2_project.entity.FileDB;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FIleDBRepository extends JpaRepository<FileDB, Long> {
    Optional<FileDB> findByName(String name);

    FileDB findByAccountId(Long id);
}
